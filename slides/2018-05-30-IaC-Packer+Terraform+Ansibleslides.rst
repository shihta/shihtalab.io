.. include:: <s5defs.txt>

============================================
 IaC - Packer + Terraform + Ansible
============================================

:Author: Shihta Kuan


Packer
======

* Packer is an open source tool for creating identical machine images for
  multiple platforms from a single source configuration.

* Supported Platforms

  VMware, EC2, Google Cloud, Azure, OpenStack, etc.


Terraform
=========

* Terraform is a tool for building, changing, and versioning infrastructure.

* Supported Platforms

  VMware vSphere, AWS, Google Cloud, Azure, OpenStack, etc.


Ansible
=======

* Ansible is an IT orchestration engine that automates configuration
  management, application deployment and many other IT needs.

* Dynamic Inventory

  Ansible can pull inventory information from cloud sources.
