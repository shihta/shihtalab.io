.. include:: <s5defs.txt>

============================================
 Remote Development Environment
============================================

:Author: Shihta Kuan


Goal
====

* Desktop environment

* GUI Tools

* Smooth


My choice
=========

* xubuntu

* vnc4server

* Visual Studio Code


Bug Fix
=======

* fix libxcb

  * sed -i 's/BIG-REQUESTS/_IG-REQUESTS/' /usr/lib/x86_64-linux-gnu/libxcb.so.1.1.0


Config: ~/.vnc/xstartup
=======================

* #!/bin/sh
* [ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
* [ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
* xsetroot -solid grey
* vncconfig -iconic &
* x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
* x-window-manager &
* :red:`startxfce4 &`


Start VNC Server
================

* vnc4server :red:`-geometry 1920x1080`


Demo
====

* https://www.youtube.com/watch?v=IOeR510DnN0&frags=pl%2Cwn

