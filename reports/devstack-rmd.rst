Installation Guide
==================

- Prepare Linux. This guide takes ubuntu 18.04 as an example.

- Add Stack User

  .. code-block:: bash

    useradd -s /bin/bash -d /opt/stack -m stack
    echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack

- Install Git: :code:`apt install git python`

- Add rocky repository: :code:`add-apt-repository ppa:ubuntu-cloud-archive/rocky-staging`

- Create folders for logging and sudo to *stack*

  .. code-block:: bash

    cd /var/log
    mkdir nova cinder neutron keystone
    chown stack:stack nova cinder neutron keystone
    su stack

- Check your permission to access Bitbucket (The following steps need to be executed by *stack* )

  - Simply clone nova once

    .. code-block:: bash

      git clone git@bitbucket.org:shihta-kuan/nova.git

  - Make sure your ssh key (~/.ssh/id_rsa.pub) is correct and this server has accepted Bitbucket's public key (~/.ssh/known_hosts)

- Download DevStack

  .. code-block:: bash

    cd ~ # make sure you are in /opt/stack
    mkdir .cache
    git clone https://shihta-kuan@bitbucket.org/shihta-kuan/devstack.git
    cd devstack
    git branch # make sure you are using "shihta/rocky"

- Create a file :code:`local.conf` in */opt/stack/devstack*. The content is shown as follows:

  - *FLAT_INTERFACE* is your network interface

  - *DATABASE_HOST* and *HOST_IP* are the IP on your network interface ( *FLAT_INTERFACE* )

  - *ADMIN_PASSWORD* is your password. As you can see, all password are the same.

  - Example

    .. code-block:: cfg

      [[local|localrc]]
      ADMIN_PASSWORD=secret
      DATABASE_PASSWORD=$ADMIN_PASSWORD
      RABBIT_PASSWORD=$ADMIN_PASSWORD
      SERVICE_PASSWORD=$ADMIN_PASSWORD
      LOGFILE=/opt/stack/devstack/stack.sh.log
      SCHEDULER_FILTERS=RetryFilter,AvailabilityZoneFilter,ComputeFilter,ComputeCapabilitiesFilter,ImagePropertiesFilter,ServerGroupAntiAffinityFilter,ServerGroupAffinityFilter,SameHostFilter,DifferentHostFilter,RDTFilter
      LIVE_MIGRATION_URI=qemu+tcp://%s/system

      CINDER_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/cinder.git
      CINDER_BRANCH=stable/rocky
      GLANCE_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/glance.git
      GLANCE_BRANCH=stable/rocky
      HORIZON_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/horizon.git
      HORIZON_BRANCH=stable/rocky
      KEYSTONE_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/keystone.git
      KEYSTONE_BRANCH=stable/rocky
      NEUTRON_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/neutron.git
      NEUTRON_BRANCH=stable/rocky
      NOVA_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/nova.git
      NOVA_BRANCH=shihta/as2
      REQUIREMENTS_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/requirements.git
      REQUIREMENTS_BRANCH=stable/rocky
      TEMPEST_REPO=https://shihta-kuan@bitbucket.org/shihta-kuan/tempest.git

      MULTI_HOST=1
      DATABASE_HOST=10.103.3.64
      HOST_IP=10.103.3.64
      FLAT_INTERFACE=ens160

      [[post-config|$NOVA_CONF]]
      [DEFAULT]
      log_dir=/var/log/nova
      use_syslog = false

      [[post-config|$CINDER_CONF]]
      [DEFAULT]
      log_dir=/var/log/cinder
      use_syslog = false

      [[post-config|$NEUTRON_CONF]]
      [DEFAULT]
      log_dir=/var/log/neutron
      use_syslog = false

      [[post-config|$KEYSTONE_CONF]]
      [DEFAULT]
      log_dir=/var/log/keystone
      use_syslog = false

- Start the install: :code:`./stack.sh`

- After the installation is complete, you can open its website.

  - Username is *admin*, and the password is what you set in the *ADMIN_PASSWORD*

- CLI credentials is *~/devstack/accrc/admin/admin*.

  - Please add below lines before you use it:

    .. code-block:: cfg

      export OS_AUTH_TYPE=password
      export OS_IDENTITY_API_VERSION=3
      alias openstack='openstack --os-placement-api-version 1.10'

  - The complete example is as follows

    .. code-block:: cfg

      # OpenStack USER ID = d7a347a9e9f24e438fd08d58dafcca5b
      export OS_USERNAME="admin"
      # OpenStack project ID = 369ef3d7564b4993892ac7401f073d4e
      export OS_PROJECT_NAME="admin"
      export OS_AUTH_URL="http://10.103.3.64/identity"
      export OS_CACERT=""
      export NOVA_CERT="/opt/stack/devstack/accrc/cacert.pem"
      export OS_PASSWORD="secret"
      export OS_USER_DOMAIN_ID=default
      unset OS_USER_DOMAIN_NAME
      export OS_PROJECT_DOMAIN_ID=default
      unset OS_PROJECT_DOMAIN_NAME
      export OS_AUTH_TYPE=password
      export OS_IDENTITY_API_VERSION=3
      alias openstack='openstack --os-placement-api-version 1.10'

- A CLI example to get inventory list

  .. code-block:: bash

    . ~/devstack/accrc/admin/admin
    openstack resource provider list
    +--------------------------------------+------------+------------+
    | uuid                                 | name       | generation |
    +--------------------------------------+------------+------------+
    | 4a6c8638-5605-4290-8649-fc11f1c138a5 | i-xu1804-0 |        117 |
    | bcd388e8-9659-4e50-8475-8a0f245738f6 | u1804-st-1 |        476 |
    | cc864d0c-a834-478f-ac00-2a8a49522375 | u1804-st-0 |       1084 |
    +--------------------------------------+------------+------------+
    openstack resource provider inventory list cc864d0c-a834-478f-ac00-2a8a49522375
    +------------------------+------------------+----------+----------+-----------+----------+--------+
    | resource_class         | allocation_ratio | max_unit | reserved | step_size | min_unit |  total |
    +------------------------+------------------+----------+----------+-----------+----------+--------+
    | VCPU                   |             16.0 |       88 |        0 |         1 |        1 |     88 |
    | MEMORY_MB              |              1.5 |   354431 |      512 |         1 |        1 | 354431 |
    | CUSTOM_CACHE_BANDWIDTH |              1.0 |      100 |        0 |        10 |       10 |   1600 |
    | CUSTOM_CACHE           |              1.0 |       11 |        0 |         1 |        1 |     22 |
    | DISK_GB                |              1.0 |    31094 |        0 |         1 |        1 |  31094 |
    +------------------------+------------------+----------+----------+-----------+----------+--------+

