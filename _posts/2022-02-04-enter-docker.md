---
layout     : post
title      : "Bash autocomplete"
subtitle   : ""
date       : 2022-02-04
author     : "Shihta"
tags       : 
comments   : true
---

* `/etc/bash_completion.d/enter-docker`:
  ```sh
  _dothis_completions()
  {
    if [ "${#COMP_WORDS[@]}" != "2" ]; then
      return
    fi
    # COMPREPLY=($(compgen -W "now tomorrow never" "${COMP_WORDS[1]}"))
    # keep the suggestions in a local variable

    local suggestions=($(compgen -W "$(docker ps --format '{{.Names}}'| tr '\n' ' '| sed 's/ *$//')" -- "${COMP_WORDS[1]}"))

    if [ "${#suggestions[@]}" == "1" ]; then
      # if there's only one match, we remove the command literal
      # to proceed with the automatic completion of the number
      local number=$(echo ${suggestions[0]/%\ */})
      COMPREPLY=("$number")
    else
      # more than one suggestions resolved,
      # respond with the suggestions intact
      COMPREPLY=("${suggestions[@]}")
    fi
  }

  complete -F _dothis_completions enter-docker shenter-docker
  ```

* `/usr/bin/enter-docker`:
  ```sh
  #!/bin/bash
  docker exec -ti $1 bash
  ```

* Reference: [Creating a bash completion script](https://iridakos.com/programming/2018/03/01/bash-programmable-completion-tutorial)
