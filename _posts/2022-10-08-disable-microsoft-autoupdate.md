---
layout     : post
title      : "Disable Microsoft Autoupdate"
subtitle   : ""
date       : 2022-10-08
author     : "Shihta"
tags       : 
comments   : true
---

Reference: [Turn off Microsoft apps 'Microsoft AutoUpdate' app on a Mac](https://superuser.com/questions/1544338/turn-off-microsoft-apps-microsoft-autoupdate-app-on-a-mac)


```
# disable the service
launchctl disable gui/$(id -u)/com.microsoft.update.agent

# check that the service is disabled
launchctl print-disabled gui/$(id -u) | grep microsoft
```
