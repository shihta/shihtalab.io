---
layout     : post
title      : "Open Safari tab in right most position"
subtitle   : ""
date       : 2022-01-13
author     : "Shihta"
tags       : 
comments   : true
---

## Steps

1. Enable Full Disk Access for Terminal before issuing the command below.
2. `defaults write com.apple.Safari IncludeInternalDebugMenu 1`
3. convert the `plist`: `plutil -convert xml1 /Users/$USER/Library/Containers/com.apple.Safari/Data/Library/Preferences/com.apple.Safari.plist`
4. adding the following two lines:
   ```
   <key>IncludeInternalDebugMenu</key>
   <true/>
   ```
5. set the `Tab Ordering`:
   ![Tab Ordering](/static/2022-01-13/safari-debug.png)

## References

- [How to disable tab preview on mouse hover in Safari for macOS?](https://apple.stackexchange.com/questions/405182/how-to-disable-tab-preview-on-mouse-hover-in-safari-for-macos)
- [Editing/amending a bplist (binary plist)](https://discussions.apple.com/thread/1768480)
- [Quick Tip: Make Safari tabs open at the end](https://sixcolors.com/post/2020/03/quick-tip-make-safari-tabs-open-at-the-end/)
- [Enabling the Debug menu in Safari 14 on Big Sur and Catalina](https://sixcolors.com/post/2020/09/enabling-the-debug-menu-in-safari-14-on-big-sur-and-catalina/)


## disable & enable ReportCrash


- disable

  ```
  launchctl unload -w /System/Library/LaunchAgents/com.apple.ReportCrash.plist
  sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.ReportCrash.Root.plist
  ```

- enable
  
  ```
  launchctl load -w /System/Library/LaunchAgents/com.apple.ReportCrash.plist
  sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.ReportCrash.Root.plist
  ```

- [reference](https://www.gregoryvarghese.com/reportcrash-high-cpu-disable-reportcrash/)
