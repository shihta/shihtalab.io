---
layout     : post
title      : "Building Repository - aptly"
subtitle   : ""
date       : 2018-05-23
author     : "Shihta"
tags       : aptly
comments   : true
---

Web server
==========

- /root/http/default.conf

  .. code-block:: bash

    server {
        listen       80;
        server_name  localhost;

        location / {
            autoindex on;
            root   /usr/share/nginx/html;
        }
    }

- Start nginx:

  .. code-block:: bash

    cd /root/http
    docker run -tid -p 80:80 -v $PWD:/usr/share/nginx/html:ro -v $PWD/default.conf:/etc/nginx/conf.d/default.conf nginx


GPG Key
=======

.. code-block:: bash

  # export private key
  gpg --list-secret-keys --keyid-format LONG
  gpg --export-secret-keys ACDE1233 > ACDE1233.asc

  # import private key
  gpg --import ACDE1233.asc


APTLY
=====

- Useful aptly commands:

  .. code-block:: bash

    aptly repo create xenial
    aptly repo create artful

    aptly repo add artful *.deb
    aptly repo add xenial *.deb

    aptly publish repo -distribution="artful" artful prefixaa
    aptly publish repo -distribution="xenial" xenial

    aptly publish update xenial
    aptly publish update artful prefixaa

    aptly repo show -with-packages artful

    aptly repo create bionic
    aptly repo add bionic *.deb
    aptly publish repo -distribution="bionic" bionic bb
    aptly publish update bionic bb

- Commands:

  .. code-block:: bash

    # Update commands
    for deb in `ls`; do aptly repo remove artful $deb; done

    # tty issue
    stty rows 50 && stty cols 150

- Server side:

  .. code-block:: bash

    docker run  -tid \
      -e FULL_NAME="Shihta Kuan" \
      -e EMAIL_ADDRESS="Shihta.Kuan@gmail.com" \
      -e GPG_PASSWORD="YOURPASSWORD" \
      -e HOSTNAME=debs.shida.info \
      -v /opt/aptly:/opt/aptly \
      -v $PWD:/debs \
      -p 81:80 \
      bryanhong/aptly:latest

- sources.list on client side:

  - xenial

    .. code-block:: bash

      wget -qO - http://debs.shida.info:81/aptly_repo_signing.key | apt-key add -

      # my.list
      deb http://debs.shida.info:81 xenial main

  - artful

    .. code-block:: bash

      wget -qO - http://debs.shida.info:81/aptly_repo_signing.key | apt-key add -

      # my.list
      deb http://debs.shida.info:81/prefixaa artful main

  - bionic

    .. code-block:: bash

      wget -qO - http://debs.shida.info:81/aptly_repo_signing.key | apt-key add -

      deb http://debs.shida.info:81/bb bionic main


RPM repository
==============

- download rpms:

  .. code-block:: bash

    base=ftp://free.nchc.org.tw/centos/7/virt/x86_64/libvirt-latest/
    set -x; \
    for n in `curl $base |grep 4.3.0| awk '{print $9}'`; do \
      wget -q $base$n; \
    done; \
    set +x

- Server side:

  .. code-block:: bash

    # prepare tools
    apt-get install createrepo

    # generate the "repomd.xml"
    cd /root/http
    createrepo ./rpm


References
==========

- `Creating a personal apt repository using dpkg-scanpackages <https://www.guyrutenberg.com/2016/07/15/creating-a-personal-apt-repository-using-dpkg-scanpackages/>`_
- `How to create a local repository for updates <https://access.redhat.com/solutions/9892>`_
- `How to Export Private / Secret ASC Key <https://stackoverflow.com/questions/5587513/how-to-export-private-secret-asc-key-to-decrypt-gpg-files-in-windows>`_
- `How To Create an Authenticated Repository <https://help.ubuntu.com/community/CreateAuthenticatedRepository>`_
- `aptly supports commands <https://www.aptly.info/doc/commands/>`_
- `docker-aptly <https://github.com/bryanhong/docker-aptly.git>`_
