---
layout     : post
title      : "Hackintosh"
subtitle   : ""
date       : 2018-09-26
author     : "Shihta"
tags       : 
comments   : true
---

Big picture
===========

- .. image:: /static/2018-09-26/big-picture.png


DSDT: Differentiated System Description Table
=============================================

- Reference: `進階組態與電源介面（英文：Advanced Configuration and Power Interface，縮寫：ACPI） <https://zh.wikipedia.org/wiki/%E9%AB%98%E7%BA%A7%E9%85%8D%E7%BD%AE%E4%B8%8E%E7%94%B5%E6%BA%90%E6%8E%A5%E5%8F%A3>`_


Clover introduction
===================

- Reference: `HotPatch开启完美黑苹果新天地！(抛弃传统DSDT方法，完美黑苹果） <https://www.weibo.com/p/23041874f0e4330102x1sy>`_

- .. image:: /static/2018-09-26/clover-architecture.jpg


Prepare your USB
================

- Reference: `[教學]看圖施工，保證成功 – 黑蘋果+Win10 雙系統主機安裝 <https://dacota.tw/blog/post/antec-cube-razer-with-macos-sierra-and-window10>`_

  1. Use unibeast
  2. Add **-v** to **boot/Arguments**
  3. You might need to enable USB inject and FixOwnership on **Devices/USB/**

- `apple-installer-checksums <https://github.com/notpeter/apple-installer-checksums>`_

- Old version(Maybe it's fake): `黑果小兵 <https://mirrors.dtops.cc/iso/MacOS/daliansky_macos/>`_

- Use **diskutil list** to show the partition

  - .. image:: /static/2018-09-26/diskutil-list.png

- Clover configuration abstract

  - .. image:: /static/2018-09-26/clover-folders.png
    .. image:: /static/2018-09-26/clover-folders-acpi.png
    .. image:: /static/2018-09-26/clover-folders-kext.png


Hardware
========

- `Best Hackintosh Laptop in 2018 <https://protechlists.com/hackintosh-laptops/>`_

  .. image:: /static/2018-09-26/hackintosh-laptops.png

- My laptop

  - Laptop model: Acer TravelMate P259-G2-M
  - CPU: Kaby Lake Mobile i5-7200U
  - GPU: Intel HD620
  - Sound Card： Realtek ALC255
  - Wifi: Intel Wireless-AC 7265
  - Ethernet: Realtek RTL8168/8111
  - Card Reader: Realtek RTS5287


My Steps
========

1. Install OS
2. Install brew
3. 膜拜 **RehabMan** `OS-X-Clover-Laptop-Config <https://github.com/RehabMan/OS-X-Clover-Laptop-Config>`_

  .. code-block:: bash

    git clone https://github.com/RehabMan/OS-X-Clover-Laptop-Config.git
    cp config_HD615_620_630_640_650.plist EFI/config.plist
    ./download.sh
    unzip _downloads/tools/iasl.zip
    cp iasl /usr/local/bin
    make
    cp Build/* EFI/CLOVER/ACPI/patched

- ls _downloads/kexts

  - RehabMan-Atheros-2015-0624.zip
  - RehabMan-Battery-2018-1005.zip # 顯示電池用量
  - RehabMan-BrcmPatchRAM-2018-0505.zip # Broadcom bluetooth
  - RehabMan-CodecCommander-2018-1003.zip
  - RehabMan-FakePCIID-2018-0919.zip
  - RehabMan-FakeSMC-2018-0915.zip
  - RehabMan-Realtek-Network-v2-2017-0322.zip
  - RehabMan-USBInjectAll-2018-1008.zip # 控制、修正USB
  - RehabMan-Voodoo-2018-1008.zip
  - acidanthera-AirportBrcmFixup.zip
  - acidanthera-BT4LEContiunityFixup.zip
  - acidanthera-Lilu.zip # 第三方driver共用套件
  - acidanthera-WhateverGreen.zip # 顯卡，需要Lilu


My kext
=======

- /S/L/E, /L/E, Clover


/Library/Extensions
~~~~~~~~~~~~~~~~~~~

- 藍牙，官網說不能放在CLOVER裡

  - BrcmPatchRAM2.kext/
  - BrcmFirmwareRepo.kext/


EFI/CLOVER/kexts/Other
~~~~~~~~~~~~~~~~~~~~~~

- ACPIBatteryManager.kext/ # 電池

- ApplePS2SmartTouchPad.kext/ # 鍵盤、touchpad

  - Do not use VoodooPS2Controller.kext and AppleSmartPS2TouchPad.kext together.

- FakeSMC.kext/ # unknow

- RtWlanU.kext/ & RtWlanU1827.kext/ # Wifi

  - From `Wireless-USB-Adapter-Clover <https://github.com/chris1111/Wireless-USB-Adapter-Clover/releases>`_
  - Need to check the path you installed, make sure it's in EFI


EFI/CLOVER/kexts/10.13
~~~~~~~~~~~~~~~~~~~~~~

- AppleALC.kext/ # 音效

  - `Supported codecs <https://github.com/acidanthera/AppleALC/wiki/Supported-codecs>`_
  - Edit your config.plist **Devices/Audio/Inject**

- CodecCommander.kext/ # unknow

- Lilu.kext/

- RealtekRTL8111.kext/ # Ethernet

- USBInjectAll.kext/ # disable local bluetooth

- VoodooI2C.kext/ # for PNLF (背光)

- WhateverGreen.kext/ # 顯卡（不知有沒有用）


mountEFI.sh
~~~~~~~~~~~

.. code-block:: bash

  DEVNAME=$(diskutil list |grep -B1 Apple |grep EFI |awk '{print $6}')
  sudo mount -t msdos /dev/$DEVNAME EFI


Apple Store Login
=================

Reference: `App Store login Verification fix for only wifi pc <https://www.tonymacx86.com/threads/hackintosh-sierra-app-store-login-verification-fix-for-only-wifi-pc.220546/>`_

1. Choose iMac
2. Makesure that your Ethernet's name is **en0**
3. Use CLover Configurator to set your SMBIOS


Disable USB port for bluetooth
==============================

Reference: `How to disable on-board Bluetooth? <https://www.reddit.com/r/hackintosh/comments/7so07s/how_to_disable_onboard_bluetooth/>`_

1. Install USBInjectAll
2. Use **SSDT-UIAC-ALL.dsl** for port configuration


Tools
=====

- `MaciASL <https://bitbucket.org/RehabMan/os-x-maciasl-patchmatic/downloads/>`_
- `tonymacx86 tools <https://www.tonymacx86.com/resources/categories/tonymacx86-downloads.3/>`_

  - MultiBeast
  - UniBeast

- `Clover Configurator <https://mackie100projects.altervista.org/download-clover-configurator/>`_
- `KCPM Utility Pro <https://goo.gl/HwYuNm>`_
- `IORegistryExplorer <https://mac.softpedia.com/get/System-Utilities/IORegistryExplorer.shtml>`_


KeyBinding
==========

- :code:`vi ./Library/KeyBindings/DefaultKeyBinding.dict`

.. code-block:: bash

  {
  /* Remap Home / End keys to be correct */
  "\UF729" = "moveToBeginningOfLine:"; /* Home */
  "\UF72B" = "moveToEndOfLine:"; /* End */
  "$\UF729" = "moveToBeginningOfLineAndModifySelection:"; /* Shift + Home */
  "$\UF72B" = "moveToEndOfLineAndModifySelection:"; /* Shift + End */
  "^\UF729" = "moveToBeginningOfDocument:"; /* Ctrl + Home */
  "^\UF72B" = "moveToEndOfDocument:"; /* Ctrl + End */
  "$^\UF729" = "moveToBeginningOfDocumentAndModifySelection:"; /* Shift + Ctrl + Home */
  "$^\UF72B" = "moveToEndOfDocumentAndModifySelection:"; /* Shift + Ctrl + End */

  "^\UF702"   = "moveToBeginningOfLine:";                       /* Home         */
  "^\UF703"   = "moveToEndOfLine:";                             /* End          */
  "$^\UF703"  = "moveToEndOfLineAndModifySelection:";           /* Shift + End  */
  "$^\UF702"  = "moveToBeginningOfLineAndModifySelection:";     /* Shift + Home */
  }


VNC key binding
===============

- `Keyboard mapping to and from a Mac <https://www.realvnc.com/en/connect/docs/mac-keyboard-mapping.html>`_
- .. image:: /static/2018-09-26/vncview-keymap.png


Bash related
============

- :code:`.bash_profile`

  .. code-block:: bash

    . .bashrc
    [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

- git

  .. code-block:: bash

    brew install git bash-completion


Fix Spotlight
=============

Reference: `Applications Don't Show Up in Spotlight <https://apple.stackexchange.com/questions/62715/applications-dont-show-up-in-spotlight>`_

.. code-block:: bash

  sudo mdutil -a -i off
  sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.metadata.mds.plist
  sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.metadata.mds.plist
  sudo mdutil -a -i on


References
==========

- `Lilu <https://github.com/acidanthera/Lilu/releases>`_
- `WhateverGreen <https://github.com/acidanthera/WhateverGreen/releases>`_
- `RehabMan/OS-X-Clover-Laptop-Config <https://github.com/RehabMan/OS-X-Clover-Laptop-Config>`_
- `我和我的黑蘋果 <https://www.facebook.com/groups/ihackintosh/learning_content/>`_


QR-Code
=======

- .. image:: /static/2018-09-26/qrcode.png