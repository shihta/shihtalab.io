---
layout     : post
title      : "Apache Traffic Control"
subtitle   : ""
date       : 2018-12-18
author     : "Shihta"
tags       : 
comments   : true
---

Development
-----------

.. code-block:: bash

  docker-compose -f docker-compose.yml -f optional/docker-compose.vpn.yml up -d vpn trafficops trafficvault trafficmonitor

.. code-block:: bash

  traffic_ops_ort -k BADASS INFO https://trafficops.infra.ciab.test:443 admin:twelve

  traffic_ops_ort -k BADASS ALL https://trafficops.infra.ciab.test:443 admin:twelve

  traffic_ctl config reload
  traffic_ctl server restart --manager

.. code-block:: bash

  # Check the CM
  curl http://10.103.21.95:3333/crs/stats/ip/60.248.18.91

.. code-block:: bash

  # CiaB
  func_ciab_compose() {
    cmd="docker-compose -f docker-compose.yml -f optional/docker-compose.static-subnet.yml -f optional/docker-compose.grafana.yml"
    if [ "$1" == "down" ] && [ "$2" == "-v" ]; then
      docker-compose exec db bash -c "cd /shared; rm -rf *"
      $cmd $@
    else
      $cmd $@
    fi
  }
  alias ciab-compose='func_ciab_compose'

  read -r -d '' CIABVAR0 <<'EOF'
  docker-compose exec trafficops /bin/bash -c "source /to-access.sh && to-get 'api/1.4/servers/' | jq '.response[] | {(.hostName):{\"ipAddress\":.ipAddress, \"ipNetmask\":.ipNetmask, \"ipGateway\":.ipGateway, \"type\":.type, \"profile\":.profile, \"cachegroup\":.cachegroup}}' | jq -s 'add'"
  EOF
  alias get-services-ip="$CIABVAR0"

  read -r -d '' CIABVAR0 <<'EOF'
  docker-compose exec trafficops /bin/bash -c "source /to-access.sh && to-get 'api/1.4/servers/' | jq '.response[]'"
  EOF
  alias get-services-ip-full="$CIABVAR0"

  alias enroll-curl="docker-compose exec enroller curl -L http://video.demo1.mycdn.ciab.test"

  alias monitor-crstates="docker-compose exec trafficmonitor curl -L http://trafficmonitor.infra.ciab.test/publish/CrStates"
  alias monitor-crstates-21="docker-compose exec trafficmonitor-21 curl -L http://trafficmonitor-21.infra.ciab.test/publish/CrStates | jq"

  alias build-ciab="make very-clean; make -j6; docker-compose build --parallel mid trafficops-perl trafficmonitor; ciab-compose build --parallel"
  alias clear-ciab-images="docker rmi `docker images | awk '/cdn-in-a-box_/{print $1}' | tr '\n' ' '`; docker rmi trafficops-go trafficops-perl trafficportal trafficmonitor"
  # CiaB end


verify the coverage-zone
------------------------

* well one great way to check is to plug the IP into the TR web interface

  .. code-block:: bash

    # router.fqdn:3333/crs/stats/ip/10.2.254.2
    curl http://10.103.21.95:3333/crs/stats/ip/60.248.18.91

    # result example:
    {
      "locationByGeo": {
        "city": "Taoyuan District",
        "countryCode": "TW",
        "latitude": "24.9889",
        "postalCode": null,
        "countryName": "Taiwan",
        "longitude": "121.3176"
      },
      "locationByFederation": "not found",
      "requestIp": "60.248.18.91",
      "locationByCoverageZone": {
        "city": null,
        "countryCode": null,
        "latitude": "25.050316",
        "postalCode": null,
        "countryName": null,
        "id": "QCT_CG_Edge_Remote_0",
        "longitude": "121.374537"
      }
    }


Influxdb
--------

* commands

  .. code-block:: bash

    SHOW RETENTION POLICIES


Private address space
---------------------

* Example IPs should be `192.0.2.*` and `2001:DB8::*`. Because:

  * `RFC5737 - IPv4 Address Blocks Reserved for Documentation <https://tools.ietf.org/html/rfc5737>`_
  * `RFC3849 - IPv6 Address Prefix Reserved for Documentation <https://tools.ietf.org/html/rfc3849>`_

* IANA is authoritative:

  * `IANA IPv4 Special-Purpose Address Registry <https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml>`_
  * `IANA IPv6 Special-Purpose Address Registry <https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml>`_

* `Address Allocation for Private Internets <https://tools.ietf.org/html/rfc1918>`_


BIND9
-----

* `Automatic empty zones (including RFC 1918 prefixes) <https://kb.isc.org/docs/aa-00800>`_
* `BIND 自 9.11.0 才會開始支援 EDNS Client Subnet <https://itbwtalk.com/2015/06/29/bind-%E8%87%AA-9-11-0-%E6%89%8D%E6%9C%83%E9%96%8B%E5%A7%8B%E6%94%AF%E6%8F%B4-edns/>`_
* `DNS Firewall Solution for BIND <https://www.isc.org/rpz/>`_
* `使用 bind9 建置混合 DNS（Response Policy Zone） <https://asaba.sakuragawa.moe/2018/03/%E4%BD%BF%E7%94%A8-bind9-%E5%BB%BA%E7%BD%AE%E6%B7%B7%E5%90%88-dns%EF%BC%88response-policy-zone%EF%BC%89/>`_


EDNS & DNSSEC
-------------

* `Testing EDNS Compatibility with dig <https://kb.isc.org/docs/edns-compatibility-dig-queries>`_
* `Using dig for testing EDNS client-subnet <https://support.opendns.com/hc/en-us/articles/227987687-Using-dig-for-testing-EDNS-client-subnet>`_
* `DNS flag day <https://dnsflagday.net/>`_
* `EDNS Compliance Tester <https://ednscomp.isc.org/ednscomp>`_
* `IP Location <https://iplocation.com/>`_


Other
-----

* `Complete List Of Country & Dialing Codes <https://www.worldatlas.com/aatlas/ctycodes.htm>`_
