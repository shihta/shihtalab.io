---
layout     : post
title      : "tc"
subtitle   : ""
date       : 2019-01-07
author     : "Shihta"
tags       : 
comments   : true
---

Set the bandwidth
=================

.. code-block:: bash

  tc qdisc add dev ens3 root handle 1: htb default 2
  tc class add dev ens3 parent 1:1 classid 1:3 htb rate 3mbit ceil 5mbit prio 2
  tc qdisc add dev ens3 parent 1:3 handle 3: sfq perturb 10
  tc filter add dev ens3 protocol ip parent 1:0 u32 match ip src 192.168.12.0/24 flowid 1:3
  # tc filter add dev ens3 protocol ip parent 1:0 u32 match ip src 192.168.122.0/24 flowid 1:3


Show current status
===================

  .. code-block:: bash

    tc qdisc show dev ens3


Reset
=====

.. code-block:: bash

  tc qdisc del dev ens3 root
