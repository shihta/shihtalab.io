---
layout     : post
title      : "vSphere notes"
subtitle   : ""
date       : 2018-04-24
author     : "Shihta"
tags       : vSphere
comments   : true
---

Using the ubuntu cloud image
============================

- ova 不論是放到 vsphere 還是 VMware Fusion 都會有問題， 單純使用 vmdk 可以開機，但是開到一半會有問題
- 使用 ovftool 時要用 Administrator 權限，範例：

  .. code-block:: bash

    ovftool --overwrite --acceptAllEulas \
      --noSSLVerify \
      --name=ubuntu-16.04-server \
      --vmFolder=RDLAB/Shihta.Kuan \
      --datastore=vsanDatastore \
      --net:"VM Network"="DPortGroup_V1303" \
      --prop:public-keys='YOUR SSH KEY' \
      ubuntu-16.04-server-cloudimg-amd64.ova \
      vi://Administrator%40vsphere.local@192.168.1.13:443/QCTRD-Datacenter/host/QCTRD

- Successful example:

  .. code-block:: bash

    ovftool --acceptAllEulas --name=cloud-image-ubuntu-18.04 \
      --datastore=vsanDatastore --noSSLVerify=true --diskMode=thick \
      --vmFolder=Shihta.Kuan --network=Network_1105 --overwrite \
      --prop:public-keys='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3/ejVwC7vatMDkYJCkanW7doGdA75w+yOwBk3vqeO39XM8hSXxcrE/KKQ+b73M93TyhXy+mwG7OdHPXcQadcQcBqlqL+oUfo+0VXQ/0SliNZll0I0itM6iCSnxr4+/3QiMFomX4G0Unbyhcwo59wTVqqNho8v2wpVFTvi++GV6W4gEudsQBa6xBKv8ICaoMj0RiHY1cYKl24VRmmzQemiWLhBLsCVzvtkR6aja7uHadZVR0Hga4hu27pUJYQa5vbRHMQ3Rq7g1ybr2h/ShO6RfYkpr0G04LfrtHdiH/HeQlA8Tcn3uV9Mom2jLOvNAAQQkZ6DBve2/F710BZpv1gH kuster@KustertekiMacBook-Air.local' \
      ubuntu-18.04-server-cloudimg-amd64.ova \
      vi://Administrator%40vsphere.local@10.101.8.49/Datacenter/host/Horizon

    ovftool --acceptAllEulas --name=test-access-u1604 \
      --datastore=vsanDatastore --noSSLVerify=true --diskMode=thick \
      --vmFolder=RDLAB/Shihta.Kuan --network=DPortGroup_V1303 --overwrite \
      --prop:public-keys='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3/ejVwC7vatMDkYJCkanW7doGdA75w+yOwBk3vqeO39XM8hSXxcrE/KKQ+b73M93TyhXy+mwG7OdHPXcQadcQcBqlqL+oUfo+0VXQ/0SliNZll0I0itM6iCSnxr4+/3QiMFomX4G0Unbyhcwo59wTVqqNho8v2wpVFTvi++GV6W4gEudsQBa6xBKv8ICaoMj0RiHY1cYKl24VRmmzQemiWLhBLsCVzvtkR6aja7uHadZVR0Hga4hu27pUJYQa5vbRHMQ3Rq7g1ybr2h/ShO6RfYkpr0G04LfrtHdiH/HeQlA8Tcn3uV9Mom2jLOvNAAQQkZ6DBve2/F710BZpv1gH kuster@KustertekiMacBook-Air.local' \
      ubuntu-16.04-server-cloudimg-amd64.ova \
      vi://shihta.kuan%40qctrd.com@192.168.1.13/QCTRD-Datacenter/host/QCTRD/Resources/RD%20lab

- Install ovftool on Linux: :code:`./VMware-ovftool-4.3.0-7948156-lin.x86_64.bundle --eulas-agreed --required`
- The real ovftool path on OSX is: :code:`/Applications/VMware OVF Tool/ovftool`.
  DON'T use :code:`/Applications/VMware Fusion.app/Contents/Library/VMware OVF Tool/ovftool`
- govc import.ova 也是有問題
- `jetbrains-infra/packer-builder-vsphere <https://github.com/jetbrains-infra/packer-builder-vsphere>`_ 的URL會拼錯不能用，但這可能是我們環境比較複雜的關係
- packer 可用， command: :code:`PACKER_LOG=1 PACKER_LOG_PATH=log.txt PACKER_KEY_INTERVAL=10ms packer build build-from-iso.js`
- packer example: `geerlingguy/packer-ubuntu-1604 <https://github.com/geerlingguy/packer-ubuntu-1604>`_
- Terraform 會出現 guest os type 不支援的狀況，但不知道為何 web 操作不會出現


vSphere VM environment
======================

dhcp server
~~~~~~~~~~~

- Installation: :code:`apt-get install isc-dhcp-server`
- Modify :code:`/etc/default/isc-dhcp-server`:

  .. code-block:: bash

    # On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
    # Separate multiple interfaces with spaces, e.g. "eth0 eth1".
    INTERFACES="ens32"

- Modify :code:`/etc/dhcp/dhcpd.conf`:

  .. code-block:: bash

    # option definitions common to all supported networks...
    option domain-name "qctrd.com";
    option domain-name-servers 192.168.1.10;

    subnet 10.103.3.0 netmask 255.255.255.0 {
      range 10.103.3.32 10.103.3.223;
      option routers 10.103.3.1;
    }

- Restart service: :code:`systemctl restart isc-dhcp-server`


Requirement
~~~~~~~~~~~

- :code:`apt-get install open-vm-tools open-vm-tools-dkms`
- Desktop needs :code:`open-vm-tools-desktop`


Packer
======

- debug mode:

  .. code-block:: bash

    ==> vmware-iso: Skipping export of virtual machine (export is allowed only for ESXi and the format needs to be specified)...
    ==> vmware-iso: Pausing after run of step 'StepExport'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepExport'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepUploadVMX'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepCleanVMX'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepConfigureVMX'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepCompactDisk'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepCleanFiles'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepShutdown'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepProvision'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepUploadTools'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepConnect'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepTypeBootCommand'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepRun'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepRegister'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepConfigureVNC'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepHTTPServer'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepSuppressMessages'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepConfigureVMX'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'stepCreateVMX'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'stepCreateDisk'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'stepRemoteUpload'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'stepRemoteUpload'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepCreateFloppy'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepOutputDir'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepDownload'. Press enter to continue.
    ==> vmware-iso: Pausing before cleanup of step 'StepPrepareTools'. Press enter to continue.
    ...........................................................................................
    ==> vmware-iso: Running post-processor: vsphere
        vmware-iso (vsphere): Uploading output-vmware-iso/packer-vmware-iso.vmx to vSphere
    Build 'vmware-iso' errored: 1 error(s) occurred:

    * Post-processor failed: Failed: exit status 1
    Opening VMX source: output-vmware-iso/packer-vmware-iso.vmx
    Error: cURL error: SSL connect error
    Completed with errors

    ==> Some builds didn't complete successfully and had errors:
    --> vmware-iso: 1 error(s) occurred:


GOVC
====

- Useful commands:

  .. code-block:: bash

    govc vm.destroy -vm.dns=u1604-n105-1

    # list info
    govc ls


References
==========

- `partman-auto-recipe.txt <https://github.com/xobs/debian-installer/blob/master/doc/devel/partman-auto-recipe.txt>`_
