---
layout     : post
title      : "reStructuredText installation"
subtitle   : ""
date       : 2018-04-20
author     : "Shihta"
tags       : rst
comments   : true
---


Installation
============

- Sublime 3
   - `Restructured​Text Improved <https://packagecontrol.io/packages/RestructuredText%20Improved>`_
   - `OmniMarkupPreviewer <https://github.com/timonwong/OmniMarkupPreviewer>`_

.. this is comments
   will not display

.. code-block:: ruby

   # Output "I love ReST"
   say = "I love ReST"
   puts say 1
   puts say 2
   puts say 3
   puts say 4
   puts say 5
   puts say 6
   puts say 7
   puts say 8
   puts say 9


.. code-block:: console
   :caption: Read Hacker News on a budget
   :url: http://news.ycombinator.com
   :title: Hacker News

   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less
   $ curl http://news.ycombinator.com | less


1. test list line 1
2. test line 2
3. line 3


- 奇怪中文語法
- 中文

Tables
------

Here's a grid table followed by a simple table:

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+----------+----------+
| body row 5             | Cells may also be     |          |
|                        | empty: ``-->``        |          |
+------------------------+-----------------------+----------+

=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
False  True   True
True   True   True
=====  =====  ======


.. contents:: Table of Contents
