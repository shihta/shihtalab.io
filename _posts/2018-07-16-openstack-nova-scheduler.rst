---
layout     : post
title      : "OpenStack Nova Scheduler"
subtitle   : ""
date       : 2018-07-16
author     : "Shihta"
tags       : 
comments   : true
---

CLI
===

# resources:CACHE=max_cache:min_cache
openstack flavor create --ram 400 --disk 1 --vcpus 2 rdt.shared
openstack flavor set --property resources:CACHE=0:0 rdt.shared

openstack flavor create --ram 400 --disk 1 --vcpus 10 rdt.guaranteed.0
openstack flavor set --property resources:CACHE=0:0 rdt.guaranteed.0
openstack flavor set --property resources:CACHE_BANDWIDTH=50 rdt.guaranteed.0
openstack flavor set --property hw:cpu_policy=dedicated --property hw:numa_nodes=1 rdt.guaranteed.0
openstack flavor set --no-property rdt.guaranteed.0

openstack flavor create --ram 400 --disk 1 --vcpus 2 rdt.guaranteed.3
openstack flavor set --property resources:CACHE=3:3 rdt.guaranteed.3
openstack flavor set --property hw:cpu_policy=dedicated --property hw:numa_nodes=1 rdt.guaranteed.3

openstack flavor create --ram 400 --disk 1 --vcpus 2 rdt.guaranteed.2
openstack flavor set --property resources:CACHE=2:2 rdt.guaranteed.2
openstack flavor set --property resources:CACHE_BANDWIDTH=50 rdt.guaranteed.2
openstack flavor set --property hw:cpu_policy=dedicated --property hw:numa_nodes=1 rdt.guaranteed.2
openstack flavor set --no-property rdt.guaranteed.2

openstack flavor create --ram 400 --disk 1 --vcpus 10 rdt.guaranteed.1
openstack flavor set --property resources:CACHE=1:1 --property hw:cpu_policy=dedicated --property hw:numa_nodes=1 rdt.guaranteed.1

openstack flavor create --ram 400 --disk 1 --vcpus 2 rdt.guaranteed.99
openstack flavor set --property resources:CACHE=99:99 rdt.guaranteed.99

openstack flavor create --ram 400 --disk 1 --vcpus 2 res.test.2
openstack flavor set --property resources:CUSTOM_CACHE=2 res.test.2

openstack flavor create --ram 400 --disk 1 --vcpus 2 rdt.shared
openstack flavor set --property resources:CACHE=0:0 rdt.shared

openstack flavor create --ram 400 --disk 1 --vcpus 6 core0
openstack flavor set --property hw:numa_nodes=1 --property hw:numa_cpus.0=0,1,2,3,4,5 core0
openstack flavor set --property hw:cpu_policy=dedicated --property hw:numa_nodes=1 core0

#########################

openstack flavor list
openstack flavor create --ram 400 --disk 1 --vcpus 2 sp0
openstack flavor set --no-property sp0

openstack flavor set --property rdt:llc=2 sp0
openstack flavor set --property rdt:mba=30 sp0
openstack flavor set --property hw:cpu_policy=dedicated sp0
openstack flavor set --property hw:numa_nodes=1 sp0

# openstack flavor set --property hw:cpu_thread_policy=require sp0

+----------------------------+--------------------------------------+
| Field                      | Value                                |
+----------------------------+--------------------------------------+
| OS-FLV-DISABLED:disabled   | False                                |
+----------------------------+--------------------------------------+
| OS-FLV-EXT-DATA:ephemeral  | 0                                    |
+----------------------------+--------------------------------------+
| access_project_ids         | None                                 |
+----------------------------+--------------------------------------+
| disk                       | 1                                    |
+----------------------------+--------------------------------------+
| id                         | c7deaeba-9a0c-4c52-921d-2c3dd3f452c1 |
+----------------------------+--------------------------------------+
| name                       | sp0                                  |
+----------------------------+--------------------------------------+
| os-flavor-access:is_public | True                                 |
+----------------------------+--------------------------------------+
| properties                 | rdt:llc='2', rdt:mba='30'            |
+----------------------------+--------------------------------------+
| ram                        | 400                                  |
+----------------------------+--------------------------------------+
| rxtx_factor                | 1.0                                  |
+----------------------------+--------------------------------------+
| swap                       |                                      |
+----------------------------+--------------------------------------+
| vcpus                      | 2                                    |
+----------------------------+--------------------------------------+

openstack server list
openstack server delete test0
openstack server create --flavor sp0 --image cirros-0.3.5-x86_64-disk test0


intel-cmt-cat
=============

# pqos -s -v
NOTE:  Mixed use of MSR and kernel interfaces to manage
       CAT or CMT & MBM may lead to unexpected behavior.
INFO: CACHE: type 1, level 1, max id sharing this cache 2 (1 bits)
INFO: CACHE: type 2, level 1, max id sharing this cache 2 (1 bits)
INFO: CACHE: type 3, level 2, max id sharing this cache 2 (1 bits)
INFO: CACHE: type 3, level 3, max id sharing this cache 64 (6 bits)
INFO: Monitoring capability detected
INFO: CPUID.0x7.0: L3 CAT supported
INFO: CDP is disabled
INFO: L3 CAT details: CDP support=1, CDP on=0, #COS=16, #ways=11, ways contention bit-mask 0x600
INFO: L3 CAT details: cache size 31719424 bytes, way size 2883584 bytes
INFO: L3CA capability detected
INFO: CPUID 0x10.0: L2 CAT not supported!
INFO: L2CA capability not detected
INFO: MBA details: #COS=8, linear, max=90, step=10
INFO: MBA capability detected
INFO: resctrl detected
INFO: OS support for CMT detected
INFO: OS support for L3 CAT detected
INFO: OS support for MBA detected
INFO: resctrl not mounted


Log - nova-scheduler
====================

DEBUG nova.scheduler.manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Starting to schedule for instances: [u'd6b9643c-f751-4bdc-8ff0-64f065aa37c2'] {{(pid=28303) select_destinations /Users/shihta.kuan/workspace/nova/nova/scheduler/manager.py:110}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "placement_client" acquired by "nova.scheduler.client.report._create_client" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "placement_client" released by "nova.scheduler.client.report._create_client" :: held 0.007s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Found 2 cells: 00000000-0000-0000-0000-000000000000, c95f288d-7cd3-4522-bf47-240c78f0d6f5 {{(pid=28303) _load_cells /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:645}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Getting compute nodes and services for cell 00000000-0000-0000-0000-000000000000(cell0) {{(pid=28303) _get_computes_for_cells /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:623}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "00000000-0000-0000-0000-000000000000" acquired by "nova.context.get_or_set_cached_cell_and_set_connections" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "00000000-0000-0000-0000-000000000000" released by "nova.context.get_or_set_cached_cell_and_set_connections" :: held 0.001s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG oslo_db.sqlalchemy.engines [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] MySQL server mode set to STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION {{(pid=28303) _check_effective_sql_mode /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_db/sqlalchemy/engines.py:308}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Getting compute nodes and services for cell c95f288d-7cd3-4522-bf47-240c78f0d6f5(cell1) {{(pid=28303) _get_computes_for_cells /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:623}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "c95f288d-7cd3-4522-bf47-240c78f0d6f5" acquired by "nova.context.get_or_set_cached_cell_and_set_connections" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "c95f288d-7cd3-4522-bf47-240c78f0d6f5" released by "nova.context.get_or_set_cached_cell_and_set_connections" :: held 0.002s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG oslo_db.sqlalchemy.engines [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] MySQL server mode set to STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION {{(pid=28303) _check_effective_sql_mode /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_db/sqlalchemy/engines.py:308}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "c95f288d-7cd3-4522-bf47-240c78f0d6f5" acquired by "nova.context.get_or_set_cached_cell_and_set_connections" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "c95f288d-7cd3-4522-bf47-240c78f0d6f5" released by "nova.context.get_or_set_cached_cell_and_set_connections" :: held 0.001s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "(u'u1604-3', u'u1604-3')" acquired by "nova.scheduler.host_manager._locked_update" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Update host state from compute node: ComputeNode(cpu_allocation_ratio=16.0,cpu_info='{"vendor": "Intel", "model": "Westmere-IBRS", "arch": "x86_64", "features": ["pge", "avx", "clflush", "sep", "syscall", "vme", "msr", "xsave", "vmx", "cmov", "ssse3", "pat", "arat", "lm", "tsc", "nx", "fxsr", "sse4.1", "pae", "sse4.2", "pclmuldq", "pcid", "mmx", "osxsave", "cx8", "mce", "de", "aes", "ht", "pse", "lahf_lm", "popcnt", "mca", "apic", "sse", "invtsc", "pni", "rdtscp", "sse2", "ss", "hypervisor", "spec-ctrl", "fpu", "cx16", "pse36", "mtrr", "x2apic"], "topology": {"cores": 2, "cells": 2, "threads": 1, "sockets": 4}}',created_at=2018-07-16T03:57:20Z,current_workload=0,deleted=False,deleted_at=None,disk_allocation_ratio=1.0,disk_available_least=183,free_disk_gb=199,free_ram_mb=31661,host='u1604-3',host_ip=10.103.3.60,hypervisor_hostname='u1604-3',hypervisor_type='QEMU',hypervisor_version=2011001,id=1,local_gb=199,local_gb_used=0,mapped=1,memory_mb=32173,memory_mb_used=512,metrics='[]',numa_topology='{"nova_object.version": "1.2", "nova_object.changes": ["cells"], "nova_object.name": "NUMATopology", "nova_object.data": {"cells": [{"nova_object.version": "1.2", "nova_object.changes": ["cpu_usage", "memory_usage", "cpuset", "mempages", "pinned_cpus", "memory", "siblings", "id"], "nova_object.name": "NUMACell", "nova_object.data": {"cpu_usage": 0, "memory_usage": 0, "cpuset": [0, 1, 2, 3, 4, 5, 6, 7], "pinned_cpus": [], "siblings": [], "memory": 16047, "mempages": [{"nova_object.version": "1.1", "nova_object.changes": ["total", "used", "reserved", "size_kb"], "nova_object.name": "NUMAPagesTopology", "nova_object.data": {"used": 0, "total": 4108072, "reserved": 0, "size_kb": 4}, "nova_object.namespace": "nova"}, {"nova_object.version": "1.1", "nova_object.changes": ["total", "used", "reserved", "size_kb"], "nova_object.name": "NUMAPagesTopology", "nova_object.data": {"used": 0, "total": 0, "reserved": 0, "size_kb": 2048}, "nova_object.namespace": "nova"}], "id": 0}, "nova_object.namespace": "nova"}, {"nova_object.version": "1.2", "nova_object.changes": ["cpu_usage", "memory_usage", "cpuset", "mempages", "pinned_cpus", "memory", "siblings", "id"], "nova_object.name": "NUMACell", "nova_object.data": {"cpu_usage": 0, "memory_usage": 0, "cpuset": [8, 9, 10, 11, 12, 13, 14, 15], "pinned_cpus": [], "siblings": [], "memory": 16126, "mempages": [{"nova_object.version": "1.1", "nova_object.changes": ["total", "used", "reserved", "size_kb"], "nova_object.name": "NUMAPagesTopology", "nova_object.data": {"used": 0, "total": 4128329, "reserved": 0, "size_kb": 4}, "nova_object.namespace": "nova"}, {"nova_object.version": "1.1", "nova_object.changes": ["total", "used", "reserved", "size_kb"], "nova_object.name": "NUMAPagesTopology", "nova_object.data": {"used": 0, "total": 0, "reserved": 0, "size_kb": 2048}, "nova_object.namespace": "nova"}], "id": 1}, "nova_object.namespace": "nova"}]}, "nova_object.namespace": "nova"}',pci_device_pools=PciDevicePoolList,ram_allocation_ratio=1.5,running_vms=0,service_id=None,stats={failed_builds='0'},supported_hv_specs=[HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec,HVSpec],updated_at=2018-07-16T05:50:27Z,uuid=84bcefa4-a838-4d73-bd3f-2f7f7e0688b7,vcpus=16,vcpus_used=0) {{(pid=28303) _locked_update /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:172}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Update host state with aggregates: [] {{(pid=28303) _locked_update /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:175}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Update host state with service dict: {'binary': u'nova-compute', 'uuid': u'8955f623-84a3-4601-be94-c4b3b2b3b3bb', 'deleted': False, 'created_at': datetime.datetime(2018, 7, 16, 3, 57, 20, tzinfo=<iso8601.Utc>), 'updated_at': datetime.datetime(2018, 7, 16, 5, 51, 11, tzinfo=<iso8601.Utc>), 'report_count': 615, 'topic': u'compute', 'host': u'u1604-3', 'version': 30, 'disabled': False, 'forced_down': False, 'last_seen_up': datetime.datetime(2018, 7, 16, 5, 51, 11, tzinfo=<iso8601.Utc>), 'deleted_at': None, 'disabled_reason': None, 'id': 2} {{(pid=28303) _locked_update /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:178}}
DEBUG nova.scheduler.host_manager [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Update host state with instances: {} {{(pid=28303) _locked_update /Users/shihta.kuan/workspace/nova/nova/scheduler/host_manager.py:181}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "(u'u1604-3', u'u1604-3')" released by "nova.scheduler.host_manager._locked_update" :: held 0.004s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Starting with 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:70}}
DEBUG nova.scheduler.filters.retry_filter [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Re-scheduling is disabled {{(pid=28303) host_passes /Users/shihta.kuan/workspace/nova/nova/scheduler/filters/retry_filter.py:38}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter RetryFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter AvailabilityZoneFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter ComputeFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter ComputeCapabilitiesFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter ImagePropertiesFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter ServerGroupAntiAffinityFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter ServerGroupAffinityFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter SameHostFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.filters [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filter DifferentHostFilter returned 1 host(s) {{(pid=28303) get_filtered_objects /Users/shihta.kuan/workspace/nova/nova/filters.py:104}}
DEBUG nova.scheduler.filter_scheduler [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Filtered [(u1604-3, u1604-3) ram: 31661MB disk: 187392MB io_ops: 0 instances: 0] {{(pid=28303) _get_sorted_hosts /Users/shihta.kuan/workspace/nova/nova/scheduler/filter_scheduler.py:404}}
DEBUG nova.scheduler.filter_scheduler [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Weighed [(u1604-3, u1604-3) ram: 31661MB disk: 187392MB io_ops: 0 instances: 0] {{(pid=28303) _get_sorted_hosts /Users/shihta.kuan/workspace/nova/nova/scheduler/filter_scheduler.py:424}}
DEBUG nova.scheduler.utils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Attempting to claim resources in the placement API for instance d6b9643c-f751-4bdc-8ff0-64f065aa37c2 {{(pid=28303) claim_resources /Users/shihta.kuan/workspace/nova/nova/scheduler/utils.py:786}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "placement_client" acquired by "nova.scheduler.client.report._create_client" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "placement_client" released by "nova.scheduler.client.report._create_client" :: held 0.004s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}
DEBUG nova.scheduler.filter_scheduler [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Selected host: (u1604-3, u1604-3) ram: 31661MB disk: 187392MB io_ops: 0 instances: 0 {{(pid=28303) _consume_selected_host /Users/shihta.kuan/workspace/nova/nova/scheduler/filter_scheduler.py:325}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "(u'u1604-3', u'u1604-3')" acquired by "nova.scheduler.host_manager._locked" :: waited 0.000s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:273}}
DEBUG oslo_concurrency.lockutils [None req-52db3715-3ca6-4bff-a473-b50414d9a5ee admin admin] Lock "(u'u1604-3', u'u1604-3')" released by "nova.scheduler.host_manager._locked" :: held 0.004s {{(pid=28303) inner /Users/shihta.kuan/.virtualenvs/nova/lib/python2.7/site-packages/oslo_concurrency/lockutils.py:285}}

