---
layout     : post
title      : "Generate Slides(S5) and PDF"
subtitle   : ""
date       : 2018-10-20
author     : "Shihta"
tags       : 
comments   : true
---

Installation
============

:code:`pip install docutils rst2pdf`


Generate slides
===============

.. code-block:: bash

  rst2s5.py slides.rst slides.html


Generate slides
===============

.. code-block:: bash

  rst2pdf devstack-rmd.rst out.pdf
