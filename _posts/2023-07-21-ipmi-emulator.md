---
layout     : post
title      : "ipmi emulator for qemu/virsh"
subtitle   : ""
date       : 2023-07-21
author     : "Shihta"
tags       : 
comments   : true
---

Refer to: `https://blog.csdn.net/shirleylinyuer/article/details/79495203`

```
<domain type='kvm' id='49' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
...
  <qemu:commandline>
    <qemu:arg value='-device'/>
    <qemu:arg value='ipmi-bmc-sim,id=bmc0'/>
    <qemu:arg value='-device'/>
    <qemu:arg value='isa-ipmi-bt,bmc=bmc0'/>
  </qemu:commandline>
</domain>
```

