---
layout     : post
title      : "City Code"
subtitle   : ""
date       : 2019-10-25
author     : "Shihta"
tags       : 
comments   : true
---

References
==========

- `City code: JP <https://www.iso.org/obp/ui/#iso:code:3166:JP>`_
- `City code: TW <https://www.iso.org/obp/ui/#iso:code:3166:TW>`_
