---
layout     : post
title      : "Packer & VMware on Ubuntu"
subtitle   : ""
date       : 2018-05-29
author     : "Shihta"
tags       : packer sharing
comments   : true
---

Environment
===========

- VMware

  .. code-block:: bash

    # vmware-installer -l
    Product Name         Product Version
    ==================== ====================
    vmware-workstation   14.1.2.8497320

    # vmware-installer -t
    Component Name                 Component Long Name                      Component Version
    ============================== ======================================== ====================
    vmware-installer               VMware Installer                                  2.1.0.7305623
    vmware-player-setup            VMware Player Setup                               14.1.2.8497320
    vmware-vmx                     VMware VMX                                        14.1.2.8497320
    vmware-network-editor          VMware Network Editor                             14.1.2.8497320
    vmware-network-editor-ui       VMware Network Editor User Interface              14.1.2.8497320
    vmware-tools-netware           VMware Tools for NetWare                          10.2.5.8497320
    vmware-tools-linuxPreGlibc25   VMware Tools for legacy Linux                     10.2.5.8497320
    vmware-tools-winPreVista       VMware Tools for Windows 2000, XP and Server 2003 10.2.5.8497320
    vmware-tools-winPre2k          VMware Tools for Windows 95, 98, Me and NT        10.2.5.8497320
    vmware-tools-freebsd           VMware Tools for FreeBSD                          10.2.5.8497320
    vmware-tools-windows           VMware Tools for Windows Vista or later           10.2.5.8497320
    vmware-tools-solaris           VMware Tools for Solaris                          10.2.5.8497320
    vmware-tools-linux             VMware Tools for Linux                            10.2.5.8497320
    vmware-player-app              VMware Player Application                         14.1.2.8497320
    vmware-workstation-server      VMware Workstation Server                         14.1.2.8497320
    vmware-ovftool                 VMware OVF Tool component for Linux               4.3.0.7948156
    vmware-vprobe                  VMware VProbes component for Linux                14.1.2.8497320
    vmware-workstation             VMware Workstation                                14.1.2.8497320

- /etc/ld.so.conf.d/vmware.conf

  .. code-block:: bash

    /usr/lib/vmware/lib/libXft.so.2
    /usr/lib/vmware/lib/libXcomposite.so.1
    /usr/lib/vmware/lib/libXfixes.so.3
    /usr/lib/vmware/lib/libXext.so.6
    /usr/lib/vmware/lib/libXdamage.so.1
    /usr/lib/vmware/lib/libXdmcp.so.6
    /usr/lib/vmware/lib/libXcursor.so.1
    /usr/lib/vmware/lib/libXtst.so.6
    /usr/lib/vmware/lib/libXrender.so.1
    /usr/lib/vmware/lib/libXi.so.6
    /usr/lib/vmware/lib/libXau.so.6
    /usr/lib/vmware/lib/libX11.so.6
    /usr/lib/vmware/lib/libXinerama.so.1
    /usr/lib/vmware/lib/libXrandr.so.2


Sharing
=======

- Run packer on u1604-n105-gitlab

  .. code-block:: bash

    PACKER_LOG=1 PACKER_LOG_PATH=log.txt PACKER_KEY_INTERVAL=10ms packer build build-from-iso.js

- Run terraform on local

  .. code-block:: bash

    terraform apply

- Run ansible on local

  .. code-block:: bash

    workon vsphere
    ansible -i vmware.py -m raw -a whoami terraform-test-* -u shihta -k


References
==========

- `VMware VIX 1.17.0 <https://my.vmware.com/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/14_0|PLAYER-1412|drivers_tools>`_
- `Download VMware Workstation Pro <https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/14_0>`_
- `Download VMware Workstation 12.5.9 Pro for Linux <https://my.vmware.com/web/vmware/details?downloadGroup=WKST-1259-LX&productId=524&rPId=20841>`_
- `Download Packer <https://www.packer.io/downloads.html>`_
- `結構文本(rst)轉網頁投影片(s5) <https://blog.gasolin.idv.tw/2007/03/16/%E7%B5%90%E6%A7%8B%E6%96%87%E6%9C%AC-rst-%E8%BD%89%E7%B6%B2%E9%A0%81%E6%8A%95%E5%BD%B1%E7%89%87-s5/>`_
- `Easy Slide Shows With reST & S5 <http://docutils.sourceforge.net/docs/user/slide-shows.html>`_
