---
layout     : post
title      : "Building dpdk"
subtitle   : ""
date       : 2018-05-04
author     : "Shihta"
tags       : dpdk
comments   : true
---

Building
========

- dpdk 17.11.1 @ xenial

.. code-block:: bash

  wget https://launchpad.net/ubuntu/+archive/primary/+files/dpdk_17.11.1-6.dsc
  wget https://launchpad.net/ubuntu/+archive/primary/+files/dpdk_17.11.1.orig.tar.xz
  wget https://launchpad.net/ubuntu/+archive/primary/+files/dpdk_17.11.1-6.debian.tar.xz

  apt-get build-dep dpdk
  apt-get install dkms libnuma-dev python3-sphinx python3-sphinx-rtd-theme

  dpkg-source -x dpdk_17.11.1-6.dsc
  cd dpdk-17.11.1
  dpkg-buildpackage -us -uc -j4

- dpdk 17.11.2 @ artful

.. code-block:: bash

  wget http://archive.ubuntu.com/ubuntu/pool/main/d/dpdk/dpdk_17.11.2-1ubuntu0.1.dsc
  wget http://archive.ubuntu.com/ubuntu/pool/main/d/dpdk/dpdk_17.11.2.orig.tar.xz
  wget http://archive.ubuntu.com/ubuntu/pool/main/d/dpdk/dpdk_17.11.2-1ubuntu0.1.debian.tar.xz

  apt-get build-dep dpdk
  apt-get install libnuma-dev python3-sphinx python3-sphinx-rtd-theme

- dpdk 18.02.1 @ artful

Build libibverbs-dev (17.1-1) first

.. code-block:: bash

  wget http://archive.ubuntu.com/ubuntu/pool/main/r/rdma-core/rdma-core_17.1-1.dsc
  wget http://archive.ubuntu.com/ubuntu/pool/main/r/rdma-core/rdma-core_17.1.orig.tar.gz
  wget http://archive.ubuntu.com/ubuntu/pool/main/r/rdma-core/rdma-core_17.1-1.debian.tar.xz
  apt-get install cmake libnl-3-dev libnl-route-3-dev libudev-dev ninja-build valgrind libibverbs-dev
  dpkg-buildpackage -us -uc -j100

After that, build dpdk

.. code-block:: bash

  apt-get install libibverbs-dev libnuma-dev
  git clone ssh://git@git0.shida.info:30022/ddp/deb_dpdk.git
  make -j100 T=x86_64-native-linuxapp-gcc
  ~/deb_dpdk/build/kmod# cp igb_uio.ko /lib/modules/4.13.0-43-generic/updates/dkms
  depmod -a


References
==========

- `dpdk 17.11.1-6 source package in Ubuntu <https://launchpad.net/ubuntu/+source/dpdk/17.11.1-6>`_
- `ConradIrwin/how-to-install-dpdk-on-ubuntu.sh <https://gist.github.com/ConradIrwin/9077440>`_
- `deb_dpdk <https://gerrit.fd.io/r/gitweb?p=deb_dpdk.git;a=summary>`_
