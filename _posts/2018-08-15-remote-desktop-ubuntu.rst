---
layout     : post
title      : "Remote desktop on ubuntu 18.04"
subtitle   : ""
date       : 2018-08-15
author     : "Shihta"
tags       : 
comments   : true
---

Building
========

.. code-block:: bash

  wget http://http.debian.net/debian/pool/main/v/vino/vino_3.14.0-2.dsc
  wget http://http.debian.net/debian/pool/main/v/vino/vino_3.14.0.orig.tar.xz
  wget http://http.debian.net/debian/pool/main/v/vino/vino_3.14.0-2.debian.tar.xz
  apt-get install cdbs libtelepathy-glib-dev network-manager-dev

  apt-get install libsoup2.4-dev libappindicator3-dev

  gsettings get org.gnome.Vino require-encryption


From server distribution to desktop distribution
================================================

.. code-block:: bash

  tasksel --list-tasks
  tasksel install ubuntu-desktop
  tasksel install xubuntu-desktop


VNC
===

vino
~~~~

- set the encryption

  .. code-block:: bash

    gsettings get org.gnome.Vino require-encryption
    gsettings set org.gnome.Vino require-encryption false

- make sure your network is managed by NetworkManager

  - /etc/netplan/01-netcfg.yaml

    .. code-block:: yaml

      network:
        version: 2
        renderer: NetworkManager

x11vnc & x0vncserver
~~~~~~~~~~~~~~~~~~~~

- both of them can redirect the DISPLAY:0

vnc4server
~~~~~~~~~~

- fix libxcb

  .. code-block:: bash

    sed -i 's/BIG-REQUESTS/_IG-REQUESTS/' /usr/lib/x86_64-linux-gnu/libxcb.so.1.1.0

- root/.vnc/xstartup

  .. code-block:: bash

    #!/bin/sh

    # Uncomment the following two lines for normal desktop:
    # unset SESSION_MANAGER
    # exec /etc/X11/xinit/xinitrc

    [ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
    [ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
    xsetroot -solid grey
    vncconfig -iconic &
    x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
    x-window-manager &
    startxfce4 &

- start vnc4server :code:`vnc4server -geometry 1920x1080`

- Reduce the VNC security :code:`vncconfig -display :1 -set BlacklistTimeout=0 -set BlacklistThreshold=1000000`


References
==========

- `Source code <https://gitlab.gnome.org/GNOME/vino>`_
- `Debian jessie version <https://packages.debian.org/jessie/vino>`_
- `vncserver too many security failures <http://www.ouvps.com/?p=1730>`_
