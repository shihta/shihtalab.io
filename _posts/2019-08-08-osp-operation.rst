---
layout     : post
title      : "OSP operation"
subtitle   : ""
date       : 2019-08-08
author     : "Shihta"
tags       : 
comments   : true
---

SSH
---

1. :code:`su - stack`
2. :code:`openstack server list`
3. :code:`ssh heat-admin@IP`

compute-0
---------

.. code-block:: bash

  [root@compute-0 net]# ll |grep 88:00
  lrwxrwxrwx. 1 root root 0 Aug  8 01:27 enp136s0f0 -> ../../devices/pci0000:85/0000:85:02.0/0000:88:00.0/net/enp136s0f0
  lrwxrwxrwx. 1 root root 0 Aug  8 01:27 enp136s0f1 -> ../../devices/pci0000:85/0000:85:02.0/0000:88:00.1/net/enp136s0f1
  lrwxrwxrwx. 1 root root 0 Aug  8 01:27 enp136s0f2 -> ../../devices/pci0000:85/0000:85:02.0/0000:88:00.2/net/enp136s0f2
  lrwxrwxrwx. 1 root root 0 Aug  8 01:27 enp136s0f3 -> ../../devices/pci0000:85/0000:85:02.0/0000:88:00.3/net/enp136s0f3
  [root@compute-0 net]# ifconfig |grep -A6 enp136s0
  enp136s0f0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
          ether 3c:fd:fe:a7:dc:50  txqueuelen 1000  (Ethernet)
          RX packets 482433  bytes 145754896 (139.0 MiB)
          RX errors 0  dropped 1822  overruns 0  frame 0
          TX packets 0  bytes 0 (0.0 B)
          TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

  enp136s0f1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
          ether 3c:fd:fe:a7:dc:51  txqueuelen 1000  (Ethernet)
          RX packets 482433  bytes 145754896 (139.0 MiB)
          RX errors 0  dropped 1822  overruns 0  frame 0
          TX packets 0  bytes 0 (0.0 B)
          TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

  enp136s0f2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
          ether 3c:fd:fe:a7:dc:52  txqueuelen 1000  (Ethernet)
          RX packets 482433  bytes 145754896 (139.0 MiB)
          RX errors 0  dropped 1822  overruns 0  frame 0
          TX packets 0  bytes 0 (0.0 B)
          TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

  enp136s0f3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
          ether 3c:fd:fe:a7:dc:53  txqueuelen 1000  (Ethernet)
          RX packets 482433  bytes 145754896 (139.0 MiB)
          RX errors 0  dropped 1822  overruns 0  frame 0
          TX packets 0  bytes 0 (0.0 B)
          TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


3c:fd:fe:a7:dc:53
3C:FD:FE:A7:DC:53


pci_passthrough:alias xxv710:2

.. code-block:: bash

  $ openstack flavor set TC \
    --property hw:cpu_policy=dedicated \
    --property pci_passthrough:alias='x710:3' \
    --property hw:numa_nodes='1'


Switch
------

.. code-block:: bash

  interface vlan 1311
  show running-config interface vlan 1311
  ip address 10.103.11.17 /24

