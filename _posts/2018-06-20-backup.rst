---
layout     : post
title      : "Backup"
subtitle   : ""
date       : 2018-06-20
author     : "Shihta"
tags       : backup
comments   : true
---

- /etc/cron.daily/gitlab-rsync

  .. code-block:: bash

    #!/bin/bash

    echo "=== START === $(date)" >> /var/log/gitlab-rsync.log
    cd /data
    rsync -zarv --del -e "ssh -p 9022" --include="*/" . 104.155.194.118:/data >> /var/log/gitlab-rsync.log 2>&1
    echo "=== END ===" >> /var/log/gitlab-rsync.log

- /etc/cron.daily/aptly-rsync

  .. code-block:: bash

    #!/bin/bash

    echo "=== START === $(date)" >> /var/log/aptly-rsync.log
    cd /opt/aptly
    rsync -zarv --del --include="*/" . 35.189.154.62:/opt/aptly >> /var/log/aptly-rsync.log 2>&1
    echo "=== END ===" >> /var/log/aptly-rsync.log
