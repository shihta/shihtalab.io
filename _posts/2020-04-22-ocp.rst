---
layout     : post
title      : "OpenShift"
subtitle   : ""
date       : 2020-04-22
author     : "Shihta"
tags       : 
comments   : true
---

Commands
--------

.. code-block::

  crictl # = docker
  podman # for images


Configuration
-------------

* Add insecure registries, edit :code:`/etc/crio/crio.conf`

  .. code-block::

    insecure_registries = [ "10.102.50.206:5000", ]


Examples
--------

.. code-block::

  systemctl restart crio
  crictl pull 10.102.50.206:5000/nfvd/telegraf:ccfa8fa9
  podman run -tid -p 59273:59273 -v $PWD/cell1-switch.conf:/etc/telegraf/telegraf.conf --log-opt max-size=10m --log-opt max-file=3 10.102.50.206:5000/nfvd/telegraf:ccfa8fa9


Expose service
--------------

.. code-block::

  oc expose service ai-cluster-master
  oc delete routes ai-cluster-master
