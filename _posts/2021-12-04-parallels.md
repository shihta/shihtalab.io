---
layout     : post
title      : "Parallels settings"
subtitle   : ""
date       : 2021-12-04
author     : "Shihta"
tags       : 
comments   : true
---

## Remove Windows OS Applications from Spotlight Search.

- System Preferences/Spotlight/Privacy Click on the "+" button and choose the directory: `/Users/shihta/Applications (Parallels)`


## Parallels Desktop 16 完美解決無法聯網，無法連線USB

1. 首先點選“前往”--“前往資料夾”，在目標路徑裡輸入`/Library/Preferences/Parallels/`，開啟資料夾後，會看到`dispatcher.desktop.xml`和`network.desktop.xml`這兩個檔案。
2. 因為許可權問題，在當前資料夾下，是無法修改檔案內容的，第一種是把這兩個檔案複製出來 ，修改完成後再替換回去 第二種使用vscode編輯
3. `dispatcher.desktop.xml`檔案，找到一行`<Usb>0</Usb>`，把這項的引數修改為`<Usb>1</Usb>`，如果找不到的話，可以使用編輯器的查詢功能來查詢這串引數，並完成修改儲存，修改完後關閉檔案。
4. `network.desktop.xml`檔案，找到第五行`<UseKextless>1</UseKextless>`或者`<UseKextless>-1</UseKextless>`，把這一行修改為`<UseKextless>0</UseKextless>`，儲存，修改完成全關閉檔案。
5. 徹底退出PD軟體，並重新開啟，可以看到網路已經完全正常了，融合模式也可以互訪桌面。

- reference: [Parallels Desktop 16 完美解決無法聯網，無法連線USB](https://www.gushiciku.cn/pl/gfTD/zh-tw)
