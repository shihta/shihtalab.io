---
layout     : post
title      : "ubuntu configs"
subtitle   : ""
date       : 2020-06-23
author     : "Shihta"
tags       : 
comments   : true
---

netplan
-------

.. code-block:: bash

  network:
    version: 2
    renderer: networkd
    ethernets:
      enp130s0f0:
        dhcp4: false
      enp130s0f1:
        dhcp4: false
    bridges:
      br0:
        interfaces: [ vlan1321 ]
        addresses: [ "10.103.21.4/24" ]
        gateway4: 10.103.21.1
        nameservers:
          addresses:
            - "192.168.1.10"
    vlans:
      vlan1321:
        id: 1321
        link: enp130s0f0
      vlan1978:
        id: 1978
        link: enp130s0f1
        addresses: [ "11.1.4.220/24" ]



Firmware Upgrade
----------------

BMC
===

- Folder name: :code:`s5bx_s5sxv4760`
- commands: :code:`./linux.sh`
- Reset IP:

  .. code-block:: bash

    ipmitool lan set 1 ipsrc static
    ipmitool lan set 1 ipaddr 10.103.15.8
    ipmitool lan set 1 netmask 255.255.255.0
    ipmitool lan set 1 defgw ipaddr 10.103.15.1

BIOS
----

- Folder name: :code:`2019WW36.5_3B14.Q304/`

