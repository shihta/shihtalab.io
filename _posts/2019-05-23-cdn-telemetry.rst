---
layout     : post
title      : "ATC telemetry"
subtitle   : ""
date       : 2019-05-23
author     : "Shihta"
tags       : 
comments   : true
---

Repo
====

* url: https://github.com/Shihta/trafficcontrol.git
* branch: master


Launch it
=========

* Some functions can be put in :code:`.bashrc`.

  .. code-block:: bash

    alias enroll-curl="docker-compose exec enroller curl -L http://video.demo1.mycdn.ciab.test"
    alias monitor-crstates="docker-compose exec trafficmonitor curl -L http://trafficmonitor.infra.ciab.test/publish/CrStates | jq"
    alias build-ciab="make very-clean; make -j6; docker-compose build"

* All of them should be run at :code:`infrastructure/cdn-in-a-box`

  * Build: :code:`build-ciab`

  * Start: :code:`docker-compose up -d`

  * Stop: :code:`docker-compose down -v`


Apache Traffic Server (ATS)
===========================

* Dockerfile: :code:`cache/Dockerfile`
* Image name: :code:`cdn-in-a-box_mid`, :code:`cdn-in-a-box_edge`
* Logs:

  .. code-block:: bash

    /var/log/ort.log
    /var/log/trafficserver/traffic.out
    /var/log/trafficserver/diags.log


Traffic Router
==============

* Dockerfile: :code:`traffic_router/Dockerfile`
* Image name: :code:`cdn-in-a-box_trafficrouter`
* Logs:

  .. code-block:: bash

    /opt/tomcat/logs/catalina.*
    /opt/traffic_router/var/log/access.log
    /opt/traffic_router/var/log/traffic_router.log


DNS (Bind9)
===========

* Dockerfile: :code:`dns/Dockerfile`
* Image name: :code:`cdn-in-a-box_dns`
* Logs: docker stdout


Traffic Monitor
===============

* Dockerfile: :code:`traffic_monitor/Dockerfile`
* Image name: :code:`trafficmonitor`
* Logs:

  .. code-block:: bash

    /opt/traffic_monitor/var/log/traffic_monitor.log


Traffic Ops - Perl
==================

* Dockerfile: :code:`traffic_ops/Dockerfile`
* Image name: :code:`trafficops-perl`
* Logs:

  .. code-block:: bash

    /var/log/traffic_ops/perl_access.log


Traffic Ops - Go
================

* Dockerfile: :code:`traffic_ops/Dockerfile-go`
* Image name: :code:`trafficops-go`
* Logs: docker stdout


Traffic Stats building
======================

* Environment

  .. code-block:: bash

    add-apt-repository ppa:gophers/archive
    apt-get update
    apt-get install golang-1.11-go



export GOPATH="/root/trafficcontrol/traffic_stats:/root/trafficcontrol/traffic_stats/vendor"
export GOPATH="/root/go:/root/go/vendor:/root/go/traffic_stats_vendor"


GOPATH=/root/go:/root/go/vendor:/root/go/traffic_stats_vendor
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/lib/go-1.11/bin:/root/go/bin
