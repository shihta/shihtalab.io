---
layout     : post
title      : "Python3 virtualenvwrapper"
subtitle   : ""
date       : 2018-09-18
author     : "Shihta"
tags       : 
comments   : true
---

Installation
============

.. code-block:: bash

  pip3 install virtualenv virtualenvwrapper


Environment Settings
====================

* ~/py3_virtenv.sh

  .. code-block:: bash

    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    source /usr/local/bin/virtualenvwrapper.sh
