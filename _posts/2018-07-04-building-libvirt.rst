---
layout     : post
title      : ""
subtitle   : ""
date       : 2018-07-04
author     : "Shihta"
tags       : 
comments   : true
---

Building
========

wireshark
---------

- Instructions

  .. code-block:: bash

    wget http://archive.ubuntu.com/ubuntu/pool/universe/w/wireshark/wireshark_2.6.1-1.dsc
    wget http://archive.ubuntu.com/ubuntu/pool/universe/w/wireshark/wireshark_2.6.1.orig.tar.xz
    wget http://archive.ubuntu.com/ubuntu/pool/universe/w/wireshark/wireshark_2.6.1-1.debian.tar.xz

    dpkg-source -x wireshark_2.6.1-1.dsc
    apt-get build-dep libwireshark-dev
    apt-get install libparse-yapp-perl libmaxminddb-dev asciidoctor liblz4-dev libsnappy-dev libspandsp-dev lynx
    dpkg-buildpackage -us -uc -j12

- Verify

  :code:`pkg-config --libs "wireshark >= 1.11.3"`


libvirt
-------

- Instructions

  .. code-block:: bash

    apt install software-properties-common
    add-apt-repository ppa:openstack-ubuntu-testing/queens

    apt-get build-dep libvirt
    apt-get install libwireshark-dev libssh2-1-dev libwiretap-dev
    wget http://http.debian.net/debian/pool/main/libv/libvirt/libvirt_4.3.0.orig.tar.xz
    dpkg-buildpackage -us -uc -j12

- 4.6.0-1 @ 18.04

  .. code-block:: bash

    apt-get build-dep libvirt-dev
    apt-get install libjansson-dev libwiretap-dev libssh2-1-dev
    dpkg-buildpackage -us -uc -j20

References
==========

- `debian libvirt source <https://salsa.debian.org/libvirt-team/libvirt>`_

