---
layout     : post
title      : "Cloudinit on OSX"
subtitle   : ""
date       : 2018-04-23
author     : "Shihta"
tags       : DevOps
comments   : true
---

write-mime-multipart
====================

.. code-block:: bash

  cd /usr/local/bin/
  wget https://raw.githubusercontent.com/lovelysystems/cloud-init/master/tools/write-mime-multipart
  chmod a+x write-mime-multipart


Modify its python path to your real path, ex: :code:`/usr/local/bin/python`

Sample command:

.. code-block:: bash

  write-mime-multipart --output=combined-userdata.txt cloudinit-gitlab.yml:text/cloud-config cloudinit-docker.sh:text/x-shellscript


References
==========

. `Q&A <https://apple.stackexchange.com/questions/125500/equivalent-of-ubuntu-s-write-mime-multipart>`_
