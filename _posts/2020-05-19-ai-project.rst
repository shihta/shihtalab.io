---
layout     : post
title      : "AI Project"
subtitle   : ""
date       : 2020-05-19
author     : "Shihta"
tags       : 
comments   : true
---


ansible-playbook -v -i ../../common-configurations/STG1/hosts.yml -e @../../STG1/vars.yml -e hostlist=stg-playform-id-db1 -e db_name=iddev mongodb-init-iddev.yml


docker load -i logstash-7.7.0.tgz


Yaml: /root/Save/setup_data_hgtro1.yaml

UHN6oskkt1cvmg0
br_api: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.80.100  netmask 255.255.255.0  broadcast 172.16.80.255
        inet6 fe80::b696:91ff:fe51:2682  prefixlen 64  scopeid 0x20<link>
        inet6 240b:c01d:157:1001::4  prefixlen 64  scopeid 0x0<global>
--
br_mgmt: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.20.88.61  netmask 255.255.255.224  broadcast 172.20.88.63
        inet6 fe80::6a05:caff:fe9b:97d8  prefixlen 64  scopeid 0x20<link>
        inet6 240b:c01d:157:1004:0:2130:0:4  prefixlen 64  scopeid 0x0<global>

SERVERS:
  UHN6hgtro1cvcm01:
    DISABLE_HYPERTHREADING: false
    cimc_info: {cimc_ip: '240b:c01d:157:1001:b6a9:fcff:fe0c:50a1', cimc_password: Sup3R@BMC,
        cimc_username: cvim}
    management_ip: 172.20.88.34
    management_ipv6: 240b:c01d:0157:1004:0000:2130:0000:1001
    rack_info: {rack_id: RackB}
  UHN6hgtro1cvcm02:
    DISABLE_HYPERTHREADING: false
    INTEL_FPGA_VFS: 2
    INTEL_VC_SRIOV_VFS: 2
    cimc_info: {cimc_ip: '240b:c01d:157:1001:b6a9:fcff:fe0b:3367', cimc_password: Sup3R@BMC,
      cimc_username: cvim}
    management_ip: 172.20.88.35
    management_ipv6: 240b:c01d:0157:1004:0000:2130:0000:1002
    rack_info: {rack_id: RackC}
  UHN6hgtro1cvcm03:
    DISABLE_HYPERTHREADING: true
    cimc_info: {cimc_ip: '240b:c01d:157:1001:b6a9:fcff:fe0b:3281', cimc_password: Sup3R@BMC,
      cimc_username: cvim}
    management_ip: 172.20.88.36
    management_ipv6: 240b:c01d:0157:1004:0000:2130:0000:1003
    rack_info: {rack_id: RackA}



rc: /root/installer-3.2.2/openstack-configs/openrc


