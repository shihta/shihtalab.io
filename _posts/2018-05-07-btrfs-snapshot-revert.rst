---
layout     : post
title      : "Btrfs snapshot / revert"
subtitle   : ""
date       : 2018-05-07
author     : "Shihta"
tags       : DevOps
comments   : true
---

How to work with snaphots in Ubuntu's layout
============================================

In order to work with snapshots of / or /home in the Ubuntu layout
it is very convenient to mount the btrfs filesystem at a separate
location, and work from the top of the btrfs tree, rather than from
the mounted subvolumes.

.. code-block:: bash

  sudo mount /dev/sdX# /mnt

Create snapshots
~~~~~~~~~~~~~~~~

To create a snapshot use

.. code-block:: bash

  sudo btrfs subvolume snapshot /mnt/@ /mnt/@_snapshot

  # or

  cd /btrfs
  btrfs subvolume snapshot @ @_snapshot

this will create a snapshot of the @ subvolume named @_snapshot
located also in the top of the btrfs tree.

Rollback to a snapshot
~~~~~~~~~~~~~~~~~~~~~~

To roll back to a snapshot, you simply need to change its name to the
name that ubuntu mounts, using

.. code-block:: bash

  sudo mv /mnt/@ /mnt/@_badroot
  sudo mv /mnt/@_snapshot /mnt/@

and reboot.

Delete a snapshot
To delete a snapshot use

.. code-block:: bash

  sudo btrfs subvolume delete /mnt/@_badroot

btrfs snapshots are subvolumes in themselves, and self-contained,
deleting the old @ subvolume like this is fine, provided we have a
replacement.

Resize
~~~~~~

.. code-block:: bash

  btrfs filesystem resize max /btrfs/@

References
==========

- https://help.ubuntu.com/community/btrfs
