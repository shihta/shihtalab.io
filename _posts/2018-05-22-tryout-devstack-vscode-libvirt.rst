---
layout     : post
title      : "Tryout: DevStack & VSCode & libvirt"
subtitle   : ""
date       : 2018-05-22
author     : "Shihta"
tags       : OpenStack VSCode
comments   : true
---

devstack
========

- sync files:

  .. code-block::

    rsync -zarv --include="*/" --include="*.py" --exclude="*" shihta.kuan@192.168.1.93:~/workspace/nova/ .; \
    uwsgi --procname-prefix placement --ini /etc/nova/placement-uwsgi.ini

- set env: :code:`source openrc admin admin`


libvirt
=======

- commands:

  ..code-block::

    virsh capabilities


Visual Studio Code
==================

- All settings are uploaded by **Settings Sync**

  - Global settings.json for **Settings Sync**

    :code:`~/Library/Application Support/Code/User/settings.json`

  - config for Settings Sync

    :code:`~/Library/Application Support/Code/User/syncLocalSettings.json`


- {WORKSPACE}/.vscode/settings.json

  .. code-block::

    {
      "python.pythonPath": "/Users/shihta.kuan/.virtualenvs/ansible/bin/python"
    }

- Linux

  .. code-block::

    apt-get install libsecret-1-0 libsecret-common libgconf-2-4 libxkbfile1 gconf2-common libgtk2.0-0 libxss1


References
==========

- `Settings Sync <https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync>`_
- `Debugging Python with VS Code <https://code.visualstudio.com/docs/python/debugging#_remote-debugging>`_
- `使用Python virtualenv时如何设置VS code <http://www.cashqian.net/blog/00148782264567542a619e58913497eb7f15af7426517ba000>`_
- `PPA queens-staging <https://launchpad.net/~ubuntu-cloud-archive/+archive/ubuntu/queens-staging>`_
- `PPA rocky-staging <https://launchpad.net/~ubuntu-cloud-archive/+archive/ubuntu/rocky-staging>`_
- `libvirt package in Ubuntu <https://launchpad.net/ubuntu/+source/libvirt>`_
- `[libvirt] [V3] RFC for support cache tune in libvirt <https://www.redhat.com/archives/libvir-list/2017-January/msg00354.html>`_
- `9.3. LIBVIRT NUMA TUNING <https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_tuning_and_optimization_guide/sect-virtualization_tuning_optimization_guide-numa-numa_and_libvirt>`_
- `Debian libvirt（4.3.0-1 <https://packages.debian.org/source/buster/libvirt>`_
- `[openstack-dev] [nova] RFC for Intel RDT/CAT Support in Nova for	Virtual Machine QoS <http://lists.openstack.org/pipermail/openstack-dev/2017-February/112691.html>`_
