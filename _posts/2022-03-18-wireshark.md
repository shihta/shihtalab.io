---
layout     : post
title      : "wireshark & tshark"
subtitle   : ""
date       : 2022-03-18
author     : "Shihta"
tags       : 
comments   : true
---

* filter

  ```
  tshark -r 5gc-0311-1433.pcap -Y "(ip.addr > 169.254.0.12 && ip.addr < 169.254.2.0 && !(sll.pkttype == 4 && udp)) || sctp" -w 5gc-0311-1433-f2.pcap
  ```