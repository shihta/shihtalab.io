---
layout     : post
title      : "CentOS base 0"
subtitle   : ""
date       : 2018-05-10
author     : "Shihta"
tags       : 
comments   : true
---

- OS Installation: Do this by selecting Install <distro> but instead of pressing *Enter*,
  press *Tab* and add *nomodeset text* to the end of the boot command line.

- /etc/sysconfig/network-scripts/ifcfg-ens192

  .. code-block:: bash

    DEVICE=ens192
    ONBOOT=yes

- Packages Installation

  .. code-block:: bash

    yum -y install vim-enhanced tree wget pciutils lshw

- /etc/bashrc

  .. code-block:: bash

    alias vi=vim
    alias tree='tree -C'
    export LC_ALL=en_US.UTF-8
    eval "$(dircolors /etc/DIR_COLORS)"
    for file in /etc/bash_completion.d/* ; do
      source "$file"
    done

- /etc/vimrc

  .. code-block:: bash

    set background=dark
    set expandtab
    set tabstop=4
    set ai
    set hls
    map <C-n> :tabnext<CR>
    map <C-p>  :tabprevious<CR>
    set tabpagemax=300
    set fencs=utf-8,big5,cp932,ucs-bom,shift-jis,gb18030,gbk,gb2312,cp936
    set laststatus=2 statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P

- Install kernel-ml

  .. code-block:: bash

    rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    yum install elrepo-release
    yum --enablerepo=elrepo-kernel install kernel-ml
    grub2-mkconfig -o /boot/grub2/grub.cfg

    # yum --enablerepo=elrepo-kernel install kernel-ml kernel-ml-devel kernel-ml-headers kernel-ml-tools kernel-ml-tools-libs kernel-ml-tools-libs-devel


Yum commands
============

.. code-block:: bash

  yum list installed
  yum --showduplicates list libvirt-daemon.x86_64


autossh
=======

.. code-block:: bash

  rpm -Uvh https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/e/epel-release-7-11.noarch.rpm
  yum install autossh


References
==========

- `Linux install goes to blank screen <https://unix.stackexchange.com/questions/353896/linux-install-goes-to-blank-screen>`_
- `How to Install or Upgrade to Kernel 4.15 in CentOS 7 <https://www.tecmint.com/install-upgrade-kernel-version-in-centos-7/>`_
