---
layout     : post
title      : ""
subtitle   : ""
date       : 2018-07-04
author     : "Shihta"
tags       : 
comments   : true
---


OpenStack status
================

- original status

  .. code-block:: bash
  
    # openstack endpoint list

  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | ID                               | Region    | Service Name | Service Type   | Enabled | Interface | URL                                         |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 32388d4fcea8477baa0d3e3cb0afa26b | RegionOne | aodh         | alarming       | True    | admin     | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 3c2f83bba9c74776951e5c9ca14739f1 | RegionOne | cinderv3     | volumev3       | True    | public    | http://10.101.5.9/volume/v3/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 3ceae6e65a404bbbb31782a84fd4c6e0 | RegionOne | cinderv2     | volumev2       | True    | public    | http://10.101.5.9/volume/v2/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 4e869b59fc754e8cb24c04381aa465fb | RegionOne | cinder       | block-storage  | True    | public    | http://10.101.5.9/volume/v3/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 4f0202a1e52e4ad5b2bd9fd3400c2e88 | RegionOne | nova         | compute        | True    | public    | http://10.101.5.9/compute/v2.1              |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 5a7ac74ac1864fc98e5f960080338deb | RegionOne | aodh         | alarming       | True    | internal  | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 6e00d93896bc4da397783e5b8dbbf40b | RegionOne | aodh         | alarming       | True    | public    | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 74f5b7591f0b4fdab3eac2b093efee8a | RegionOne | nova_legacy  | compute_legacy | True    | public    | http://10.101.5.9/compute/v2/$(project_id)s |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 7babf4da5e144fe39044cb44665f4027 | RegionOne | glance       | image          | True    | public    | http://10.101.5.9/image                     |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 7f084e7040fc424ca69077182b4f320d | RegionOne | neutron      | network        | True    | public    | http://10.101.5.9:9696/                     |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 8b0a96886fdd49c3841746d2d07417ee | RegionOne | cinder       | volume         | True    | public    | http://10.101.5.9/volume/v1/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | a9254c62bb7a41f5b216c715e808d67d | RegionOne | gnocchi      | metric         | True    | internal  | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | bb54962ac5b94c03929f72a5129ef47e | RegionOne | placement    | placement      | True    | public    | http://10.101.5.9/placement                 |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | d80ca84aec2440319776759d3f35148d | RegionOne | keystone     | identity       | True    | public    | http://10.101.5.9/identity                  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | e47d5e0db14f47f6af3162fe21dce2e7 | RegionOne | gnocchi      | metric         | True    | public    | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | f2175a5fedae4e0582823b3f4d42a6bc | RegionOne | keystone     | identity       | True    | admin     | http://10.101.5.9/identity                  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | f6e8c4996b7a4285baf9d987c7568482 | RegionOne | gnocchi      | metric         | True    | admin     | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+

- current status

  .. code-block:: bash

    openstack endpoint set --url http://192.168.1.93:8000 bb54962ac5b94c03929f72a5129ef47e

  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | ID                               | Region    | Service Name | Service Type   | Enabled | Interface | URL                                         |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 32388d4fcea8477baa0d3e3cb0afa26b | RegionOne | aodh         | alarming       | True    | admin     | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 3c2f83bba9c74776951e5c9ca14739f1 | RegionOne | cinderv3     | volumev3       | True    | public    | http://10.101.5.9/volume/v3/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 3ceae6e65a404bbbb31782a84fd4c6e0 | RegionOne | cinderv2     | volumev2       | True    | public    | http://10.101.5.9/volume/v2/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 4e869b59fc754e8cb24c04381aa465fb | RegionOne | cinder       | block-storage  | True    | public    | http://10.101.5.9/volume/v3/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 4f0202a1e52e4ad5b2bd9fd3400c2e88 | RegionOne | nova         | compute        | True    | public    | http://10.101.5.9/compute/v2.1              |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 5a7ac74ac1864fc98e5f960080338deb | RegionOne | aodh         | alarming       | True    | internal  | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 6e00d93896bc4da397783e5b8dbbf40b | RegionOne | aodh         | alarming       | True    | public    | http://10.101.5.9:8042                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 74f5b7591f0b4fdab3eac2b093efee8a | RegionOne | nova_legacy  | compute_legacy | True    | public    | http://10.101.5.9/compute/v2/$(project_id)s |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 7babf4da5e144fe39044cb44665f4027 | RegionOne | glance       | image          | True    | public    | http://10.101.5.9/image                     |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 7f084e7040fc424ca69077182b4f320d | RegionOne | neutron      | network        | True    | public    | http://10.101.5.9:9696/                     |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | 8b0a96886fdd49c3841746d2d07417ee | RegionOne | cinder       | volume         | True    | public    | http://10.101.5.9/volume/v1/$(project_id)s  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | a9254c62bb7a41f5b216c715e808d67d | RegionOne | gnocchi      | metric         | True    | internal  | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | bb54962ac5b94c03929f72a5129ef47e | RegionOne | placement    | placement      | True    | public    | http://192.168.1.93:8000                    |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | d80ca84aec2440319776759d3f35148d | RegionOne | keystone     | identity       | True    | public    | http://10.101.5.9/identity                  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | e47d5e0db14f47f6af3162fe21dce2e7 | RegionOne | gnocchi      | metric         | True    | public    | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | f2175a5fedae4e0582823b3f4d42a6bc | RegionOne | keystone     | identity       | True    | admin     | http://10.101.5.9/identity                  |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+
  | f6e8c4996b7a4285baf9d987c7568482 | RegionOne | gnocchi      | metric         | True    | admin     | http://10.101.5.9:8041                      |
  +----------------------------------+-----------+--------------+----------------+---------+-----------+---------------------------------------------+


commands
========

Command "resource" matches:
  resource class create
  resource class delete
  resource class list
  resource class show
  resource provider aggregate list
  resource provider aggregate set
  resource provider allocation delete
  resource provider allocation set
  resource provider allocation show
  resource provider create
  resource provider delete
  resource provider inventory class set
  resource provider inventory delete
  resource provider inventory list
  resource provider inventory set
  resource provider inventory show
  resource provider list
  resource provider set
  resource provider show
  resource provider usage show

- source_admin, use the **Maximum in Pike**

  .. code-block:: bash

    export OS_AUTH_TYPE=password
    export OS_IDENTITY_API_VERSION=3
    export OS_COMPUTE_API_VERSION=2.53
    alias openstack='openstack --os-placement-api-version 1.10'

- openstack hypervisor list

  +--------------------------------------+---------------------+-----------------+-------------+-------+
  | ID                                   | Hypervisor Hostname | Hypervisor Type | Host IP     | State |
  +--------------------------------------+---------------------+-----------------+-------------+-------+
  | 382f3758-a71d-4c62-8d86-ca2d800dbe0a | u1604d-1            | QEMU            | 10.103.3.61 | up    |
  +--------------------------------------+---------------------+-----------------+-------------+-------+
  | e2e21003-3df7-48d3-8d81-03570d9826f8 | u1604d-2            | QEMU            | 10.103.3.62 | up    |
  +--------------------------------------+---------------------+-----------------+-------------+-------+

- openstack server create --flavor m1.tiny --image cirros-0.3.5-x86_64-disk test2

- openstack server delete test2

- openstack resource provider usage show ea580606-2f51-4eeb-83fb-64b7bca41d47

  +----------------+-------+
  | resource_class | usage |
  +----------------+-------+
  | VCPU           |     2 |
  +----------------+-------+
  | MEMORY_MB      |  1024 |
  +----------------+-------+
  | DISK_GB        |     2 |
  +----------------+-------+

- openstack resource provider inventory list e2e21003-3df7-48d3-8d81-03570d9826f8

  +----------------+------------------+----------+----------+-----------+----------+-------+
  | resource_class | allocation_ratio | max_unit | reserved | step_size | min_unit | total |
  +----------------+------------------+----------+----------+-----------+----------+-------+
  | VCPU           |             16.0 |        4 |        0 |         1 |        1 |     4 |
  +----------------+------------------+----------+----------+-----------+----------+-------+
  | MEMORY_MB      |              1.5 |     7977 |      512 |         1 |        1 |  7977 |
  +----------------+------------------+----------+----------+-----------+----------+-------+
  | DISK_GB        |              1.0 |      159 |        0 |         1 |        1 |   159 |
  +----------------+------------------+----------+----------+-----------+----------+-------+


Unit test
=========

- Commands

  .. code-block:: bash

    python -m testtools.run -v nova.tests.unit.compute.test_resource_tracker.TestInitComputeNode
    tox -e py27 -- TestInitComputeNode


Documents
=========

- `<https://github.com/openstack/openstack-manuals>`_ @ ubuntu 18.04

  .. code-block:: bash

    apt install tox gcc libdpkg-perl python3-dev libpcre3-dev make latexmk texlive-xetex fonts-liberation fonts-liberation2
    tox -e build -- install-guide --pdf

  - `<https://github.com/openstack/openstack-ansible>`_ @ ubuntu 18.04

    .. code-block:: bash

      apt-get install python3-pip
      pip3 install sphinx openstackdocstheme sphinxmark

- `CLI Mapping Guide <https://docs.openstack.org/python-openstackclient/pike/cli/decoder.html>`_

- `Install and configure a compute node for Ubuntu <https://docs.openstack.org/nova/queens/install/compute-install-ubuntu.html>`_

  .. code-block:: bash

    openstack compute service list --service nova-compute
    nova-manage cell_v2 discover_hosts --verbose


Placement
=========

.. code-block:: bash

  # Have to match '^CUSTOM\\_[A-Z0-9_]+$'
  openstack resource class create CUSTOM_CACHE

  openstack resource provider inventory class set \
    --total 10 --min_unit 1 --step_size 1 --reserved 0 --max_unit 10 --allocation_ratio 1.0 \
    84bcefa4-a838-4d73-bd3f-2f7f7e0688b7 CUSTOM_CACHE

  #############################

  openstack resource class create CUSTOM_CACHE_GUARANTEE_CPU_0
  openstack resource class create CUSTOM_CACHE_GUARANTEE_CPU_1
  openstack resource class create CUSTOM_CACHE_SHARED_CPU_0
  openstack resource class create CUSTOM_CACHE_SHARED_CPU_1

  openstack resource provider inventory set \
    --total 6 --min_unit 1 --step_size 1 --reserved 0 --max_unit 6 --allocation_ratio 1.0 \
    1a747270-2506-4b80-8e63-366ae7f6abb4 CUSTOM_CACHE_GUARANTEE_CPU_0

  1a747270-2506-4b80-8e63-366ae7f6abb4 CUSTOM_CACHE_GUARANTEE_CPU_1
  1a747270-2506-4b80-8e63-366ae7f6abb4 CUSTOM_CACHE_SHARED_CPU_0
  1a747270-2506-4b80-8e63-366ae7f6abb4 CUSTOM_CACHE_SHARED_CPU_1


References
==========

- `nova api version history <https://docs.openstack.org/nova/latest/reference/api-microversion-history.html>`_
- `placement api version history <https://docs.openstack.org/nova/queens/user/placement.html>`_


Log
===

- delete vm

  .. code-block:: bash

    10.101.5.9 - - [05/Jul/2018 14:21:19] "DELETE /allocations/f6a67296-0854-4939-95ef-1d3c7d0bfa84 HTTP/1.1" 204 0

    DEBUG nova.api.openstack.placement.handlers.allocation [req-f78c8fa4-8116-4013-b569-f0eae28a34c1 req-947652c7-f7d8-4e84-8d54-c3950d39df1d service placement] Successfully deleted allocations AllocationList[Allocation(consumer_id=1f7e8380-a644-41e7-b7f4-bbd1e50f0f49,created_at=<?>,id=<?>,project_id='bd90ee38d54b4c5c88a32a8799995871',resource_class='MEMORY_MB',resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),updated_at=<?>,used=512,user_id='c993a511daec4678813e158e57eb7772'), Allocation(consumer_id=1f7e8380-a644-41e7-b7f4-bbd1e50f0f49,created_at=<?>,id=<?>,project_id='bd90ee38d54b4c5c88a32a8799995871',resource_class='VCPU',resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),updated_at=<?>,used=1,user_id='c993a511daec4678813e158e57eb7772'), Allocation(consumer_id=1f7e8380-a644-41e7-b7f4-bbd1e50f0f49,created_at=<?>,id=<?>,project_id='bd90ee38d54b4c5c88a32a8799995871',resource_class='DISK_GB',resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),updated_at=<?>,used=1,user_id='c993a511daec4678813e158e57eb7772')] {{(pid=26369) delete_allocations /Users/shihta.kuan/workspace/nova/nova/api/openstack/placement/handlers/allocation.py:396}}
    INFO nova.api.openstack.placement.requestlog [req-f78c8fa4-8116-4013-b569-f0eae28a34c1 req-947652c7-f7d8-4e84-8d54-c3950d39df1d service placement] 10.101.5.9 "DELETE /allocations/1f7e8380-a644-41e7-b7f4-bbd1e50f0f49" status: 204 len: 0 microversion: 1.0

- create vm

>>>>> _get_by_requests req:
suffix =
request = RequestGroup(use_same_provider=False, resources={DISK_GB:1, MEMORY_MB:512, VCPU:1}, traits=[], aggregates=[]) <class 'nova.api.openstack.placement.lib.RequestGroup'>
candidates = {'': ([AllocationRequest(anchor_root_provider_uuid=ea580606-2f51-4eeb-83fb-64b7bca41d47,resource_requests=[AllocationRequestResource,AllocationRequestResource,AllocationRequestResource],use_same_provider=False)], [ProviderSummary(resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),resources=[ProviderSummaryResource,ProviderSummaryResource,ProviderSummaryResource],traits=[])])}
alloc_request_objs = [AllocationRequest(anchor_root_provider_uuid=ea580606-2f51-4eeb-83fb-64b7bca41d47,resource_requests=[AllocationRequestResource,AllocationRequestResource,AllocationRequestResource],use_same_provider=<?>)]
summary_objs = [ProviderSummary(resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),resources=[ProviderSummaryResource,ProviderSummaryResource,ProviderSummaryResource],traits=[])]
>>>>>
alloc_request_objs <type 'list'>
AllocationRequest(anchor_root_provider_uuid=ea580606-2f51-4eeb-83fb-64b7bca41d47,resource_requests=[AllocationRequestResource,AllocationRequestResource,AllocationRequestResource],use_same_provider=<?>) <class 'nova.api.openstack.placement.objects.resource_provider.AllocationRequest'>
kept_summary_objs <type 'list'>
ProviderSummary(resource_provider=ResourceProvider(ea580606-2f51-4eeb-83fb-64b7bca41d47),resources=[ProviderSummaryResource,ProviderSummaryResource,ProviderSummaryResource],traits=[]) <class 'nova.api.openstack.placement.objects.resource_provider.ProviderSummary'>
<<<<<
10.101.5.9 - - [05/Jul/2018 14:19:37] "GET /allocation_candidates?resources=DISK_GB%3A1%2CMEMORY_MB%3A512%2CVCPU%3A1&limit=1000 HTTP/1.1" 200 364
10.101.5.9 - - [05/Jul/2018 14:19:37] "GET /allocations/f6a67296-0854-4939-95ef-1d3c7d0bfa84 HTTP/1.1" 200 19
10.101.5.9 - - [05/Jul/2018 14:19:37] "PUT /allocations/f6a67296-0854-4939-95ef-1d3c7d0bfa84 HTTP/1.1" 204 0
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers?in_tree=ea580606-2f51-4eeb-83fb-64b7bca41d47 HTTP/1.1" 200 801
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/aggregates HTTP/1.1" 200 18
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/traits HTTP/1.1" 200 49
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers?in_tree=ea580606-2f51-4eeb-83fb-64b7bca41d47 HTTP/1.1" 200 801
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
10.101.5.9 - - [05/Jul/2018 14:19:38] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/aggregates HTTP/1.1" 200 18
10.101.5.9 - - [05/Jul/2018 14:19:39] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/traits HTTP/1.1" 200 49
10.101.5.9 - - [05/Jul/2018 14:19:39] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
10.101.5.9 - - [05/Jul/2018 14:19:39] "GET /allocations/f6a67296-0854-4939-95ef-1d3c7d0bfa84 HTTP/1.1" 200 134

127.0.0.1 - - [04/Jul/2018 15:21:12] "GET /allocation_candidates?resources=DISK_GB%3A1%2CMEMORY_MB%3A512%2CVCPU%3A1&limit=1000 HTTP/1.1" 200 362
127.0.0.1 - - [04/Jul/2018 15:21:12] "GET /allocations/8be75f4f-be05-477b-99e7-3fdb24b33009 HTTP/1.1" 200 19
127.0.0.1 - - [04/Jul/2018 15:21:13] "PUT /allocations/8be75f4f-be05-477b-99e7-3fdb24b33009 HTTP/1.1" 204 0
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers?in_tree=ea580606-2f51-4eeb-83fb-64b7bca41d47 HTTP/1.1" 200 801
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/aggregates HTTP/1.1" 200 18
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/traits HTTP/1.1" 200 49
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers?in_tree=ea580606-2f51-4eeb-83fb-64b7bca41d47 HTTP/1.1" 200 801
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/aggregates HTTP/1.1" 200 18
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/traits HTTP/1.1" 200 49
127.0.0.1 - - [04/Jul/2018 15:21:13] "GET /resource_providers/ea580606-2f51-4eeb-83fb-64b7bca41d47/inventories HTTP/1.1" 200 397
127.0.0.1 - - [04/Jul/2018 15:21:19] "GET /allocations/8be75f4f-be05-477b-99e7-3fdb24b33009 HTTP/1.1" 200 134


class AllocationCandidates(base.VersionedObject):
    """The AllocationCandidates object is a collection of possible allocations
    that match some request for resources, along with some summary information
    about the resource providers involved in these allocation candidates.
    """

